#include "cytypes.h"


void Delay (unsigned long a);
void CSN_HIGH (void);
void CSN_LOW (void);
void CE_HIGH(void);
void CE_LOW(void);
void SPI0Init(void);
uint8 SPI_SendByte(uint8 myData);
uint8 SPI_Send_command_with_ADDR (uint8 cmd, uint8 addr, uint8 data_byte);
uint8 SPI_Send_command_without_ADDR (uint8 cmd, uint8 data_byte);
// Setting for nRF chip
void nRFTXSetting(void);
void nRF_Test(void);


