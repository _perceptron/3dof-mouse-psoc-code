/*******************************************************************************
* File Name: PWM_ADC_Sampler_PM.c
* Version 3.0
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/
#include "cytypes.h"
#include "PWM_ADC_Sampler.h"

static PWM_ADC_Sampler_backupStruct PWM_ADC_Sampler_backup;


/*******************************************************************************
* Function Name: PWM_ADC_Sampler_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_ADC_Sampler_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void PWM_ADC_Sampler_SaveConfig(void) 
{
    
    #if(!PWM_ADC_Sampler_UsingFixedFunction)
        #if(!PWM_ADC_Sampler_PWMModeIsCenterAligned)
            PWM_ADC_Sampler_backup.PWMPeriod = PWM_ADC_Sampler_ReadPeriod();
        #endif /* (!PWM_ADC_Sampler_PWMModeIsCenterAligned) */
        PWM_ADC_Sampler_backup.PWMUdb = PWM_ADC_Sampler_ReadCounter();
        #if (PWM_ADC_Sampler_UseStatus)
            PWM_ADC_Sampler_backup.InterruptMaskValue = PWM_ADC_Sampler_STATUS_MASK;
        #endif /* (PWM_ADC_Sampler_UseStatus) */
        
        #if(PWM_ADC_Sampler_DeadBandMode == PWM_ADC_Sampler__B_PWM__DBM_256_CLOCKS || \
            PWM_ADC_Sampler_DeadBandMode == PWM_ADC_Sampler__B_PWM__DBM_2_4_CLOCKS)
            PWM_ADC_Sampler_backup.PWMdeadBandValue = PWM_ADC_Sampler_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */
        
        #if(PWM_ADC_Sampler_KillModeMinTime)
             PWM_ADC_Sampler_backup.PWMKillCounterPeriod = PWM_ADC_Sampler_ReadKillTime();
        #endif /* (PWM_ADC_Sampler_KillModeMinTime) */
        
        #if(PWM_ADC_Sampler_UseControl)
            PWM_ADC_Sampler_backup.PWMControlRegister = PWM_ADC_Sampler_ReadControlRegister();
        #endif /* (PWM_ADC_Sampler_UseControl) */
    #endif  /* (!PWM_ADC_Sampler_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PWM_ADC_Sampler_RestoreConfig
********************************************************************************
* 
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_ADC_Sampler_backup:  Variables of this global structure are used to  
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_ADC_Sampler_RestoreConfig(void) 
{
        #if(!PWM_ADC_Sampler_UsingFixedFunction)
            #if(!PWM_ADC_Sampler_PWMModeIsCenterAligned)
                PWM_ADC_Sampler_WritePeriod(PWM_ADC_Sampler_backup.PWMPeriod);
            #endif /* (!PWM_ADC_Sampler_PWMModeIsCenterAligned) */
            PWM_ADC_Sampler_WriteCounter(PWM_ADC_Sampler_backup.PWMUdb);
            #if (PWM_ADC_Sampler_UseStatus)
                PWM_ADC_Sampler_STATUS_MASK = PWM_ADC_Sampler_backup.InterruptMaskValue;
            #endif /* (PWM_ADC_Sampler_UseStatus) */
            
            #if(PWM_ADC_Sampler_DeadBandMode == PWM_ADC_Sampler__B_PWM__DBM_256_CLOCKS || \
                PWM_ADC_Sampler_DeadBandMode == PWM_ADC_Sampler__B_PWM__DBM_2_4_CLOCKS)
                PWM_ADC_Sampler_WriteDeadTime(PWM_ADC_Sampler_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */
            
            #if(PWM_ADC_Sampler_KillModeMinTime)
                PWM_ADC_Sampler_WriteKillTime(PWM_ADC_Sampler_backup.PWMKillCounterPeriod);
            #endif /* (PWM_ADC_Sampler_KillModeMinTime) */
            
            #if(PWM_ADC_Sampler_UseControl)
                PWM_ADC_Sampler_WriteControlRegister(PWM_ADC_Sampler_backup.PWMControlRegister); 
            #endif /* (PWM_ADC_Sampler_UseControl) */
        #endif  /* (!PWM_ADC_Sampler_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: PWM_ADC_Sampler_Sleep
********************************************************************************
* 
* Summary:
*  Disables block's operation and saves the user configuration. Should be called 
*  just prior to entering sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_ADC_Sampler_backup.PWMEnableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void PWM_ADC_Sampler_Sleep(void) 
{
    #if(PWM_ADC_Sampler_UseControl)
        if(PWM_ADC_Sampler_CTRL_ENABLE == (PWM_ADC_Sampler_CONTROL & PWM_ADC_Sampler_CTRL_ENABLE))
        {
            /*Component is enabled */
            PWM_ADC_Sampler_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            PWM_ADC_Sampler_backup.PWMEnableState = 0u;
        }
    #endif /* (PWM_ADC_Sampler_UseControl) */

    /* Stop component */
    PWM_ADC_Sampler_Stop();
    
    /* Save registers configuration */
    PWM_ADC_Sampler_SaveConfig();
}


/*******************************************************************************
* Function Name: PWM_ADC_Sampler_Wakeup
********************************************************************************
* 
* Summary:
*  Restores and enables the user configuration. Should be called just after 
*  awaking from sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_ADC_Sampler_backup.pwmEnable:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_ADC_Sampler_Wakeup(void) 
{
     /* Restore registers values */
    PWM_ADC_Sampler_RestoreConfig();
    
    if(PWM_ADC_Sampler_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        PWM_ADC_Sampler_Enable();
    } /* Do nothing if component's block was disabled before */
    
}


/* [] END OF FILE */
