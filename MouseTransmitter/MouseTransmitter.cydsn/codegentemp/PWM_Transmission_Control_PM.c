/*******************************************************************************
* File Name: PWM_Transmission_Control_PM.c
* Version 3.0
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/
#include "cytypes.h"
#include "PWM_Transmission_Control.h"

static PWM_Transmission_Control_backupStruct PWM_Transmission_Control_backup;


/*******************************************************************************
* Function Name: PWM_Transmission_Control_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_Transmission_Control_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void PWM_Transmission_Control_SaveConfig(void) 
{
    
    #if(!PWM_Transmission_Control_UsingFixedFunction)
        #if(!PWM_Transmission_Control_PWMModeIsCenterAligned)
            PWM_Transmission_Control_backup.PWMPeriod = PWM_Transmission_Control_ReadPeriod();
        #endif /* (!PWM_Transmission_Control_PWMModeIsCenterAligned) */
        PWM_Transmission_Control_backup.PWMUdb = PWM_Transmission_Control_ReadCounter();
        #if (PWM_Transmission_Control_UseStatus)
            PWM_Transmission_Control_backup.InterruptMaskValue = PWM_Transmission_Control_STATUS_MASK;
        #endif /* (PWM_Transmission_Control_UseStatus) */
        
        #if(PWM_Transmission_Control_DeadBandMode == PWM_Transmission_Control__B_PWM__DBM_256_CLOCKS || \
            PWM_Transmission_Control_DeadBandMode == PWM_Transmission_Control__B_PWM__DBM_2_4_CLOCKS)
            PWM_Transmission_Control_backup.PWMdeadBandValue = PWM_Transmission_Control_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */
        
        #if(PWM_Transmission_Control_KillModeMinTime)
             PWM_Transmission_Control_backup.PWMKillCounterPeriod = PWM_Transmission_Control_ReadKillTime();
        #endif /* (PWM_Transmission_Control_KillModeMinTime) */
        
        #if(PWM_Transmission_Control_UseControl)
            PWM_Transmission_Control_backup.PWMControlRegister = PWM_Transmission_Control_ReadControlRegister();
        #endif /* (PWM_Transmission_Control_UseControl) */
    #endif  /* (!PWM_Transmission_Control_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PWM_Transmission_Control_RestoreConfig
********************************************************************************
* 
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_Transmission_Control_backup:  Variables of this global structure are used to  
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_Transmission_Control_RestoreConfig(void) 
{
        #if(!PWM_Transmission_Control_UsingFixedFunction)
            #if(!PWM_Transmission_Control_PWMModeIsCenterAligned)
                PWM_Transmission_Control_WritePeriod(PWM_Transmission_Control_backup.PWMPeriod);
            #endif /* (!PWM_Transmission_Control_PWMModeIsCenterAligned) */
            PWM_Transmission_Control_WriteCounter(PWM_Transmission_Control_backup.PWMUdb);
            #if (PWM_Transmission_Control_UseStatus)
                PWM_Transmission_Control_STATUS_MASK = PWM_Transmission_Control_backup.InterruptMaskValue;
            #endif /* (PWM_Transmission_Control_UseStatus) */
            
            #if(PWM_Transmission_Control_DeadBandMode == PWM_Transmission_Control__B_PWM__DBM_256_CLOCKS || \
                PWM_Transmission_Control_DeadBandMode == PWM_Transmission_Control__B_PWM__DBM_2_4_CLOCKS)
                PWM_Transmission_Control_WriteDeadTime(PWM_Transmission_Control_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */
            
            #if(PWM_Transmission_Control_KillModeMinTime)
                PWM_Transmission_Control_WriteKillTime(PWM_Transmission_Control_backup.PWMKillCounterPeriod);
            #endif /* (PWM_Transmission_Control_KillModeMinTime) */
            
            #if(PWM_Transmission_Control_UseControl)
                PWM_Transmission_Control_WriteControlRegister(PWM_Transmission_Control_backup.PWMControlRegister); 
            #endif /* (PWM_Transmission_Control_UseControl) */
        #endif  /* (!PWM_Transmission_Control_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: PWM_Transmission_Control_Sleep
********************************************************************************
* 
* Summary:
*  Disables block's operation and saves the user configuration. Should be called 
*  just prior to entering sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_Transmission_Control_backup.PWMEnableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void PWM_Transmission_Control_Sleep(void) 
{
    #if(PWM_Transmission_Control_UseControl)
        if(PWM_Transmission_Control_CTRL_ENABLE == (PWM_Transmission_Control_CONTROL & PWM_Transmission_Control_CTRL_ENABLE))
        {
            /*Component is enabled */
            PWM_Transmission_Control_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            PWM_Transmission_Control_backup.PWMEnableState = 0u;
        }
    #endif /* (PWM_Transmission_Control_UseControl) */

    /* Stop component */
    PWM_Transmission_Control_Stop();
    
    /* Save registers configuration */
    PWM_Transmission_Control_SaveConfig();
}


/*******************************************************************************
* Function Name: PWM_Transmission_Control_Wakeup
********************************************************************************
* 
* Summary:
*  Restores and enables the user configuration. Should be called just after 
*  awaking from sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_Transmission_Control_backup.pwmEnable:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_Transmission_Control_Wakeup(void) 
{
     /* Restore registers values */
    PWM_Transmission_Control_RestoreConfig();
    
    if(PWM_Transmission_Control_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        PWM_Transmission_Control_Enable();
    } /* Do nothing if component's block was disabled before */
    
}


/* [] END OF FILE */
