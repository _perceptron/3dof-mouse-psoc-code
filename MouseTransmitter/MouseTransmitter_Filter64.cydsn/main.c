/* ========================================
nRF24L01 specifics 
* ========================================
SPI response should be 0x0E - reset value
CE Pin controls TX and RX on the chip. Active high.
*/


#include <project.h>
#include <stdlib.h>
#include "nRF24L01.h"
#include "const.h"
#include "uartCalls.h"
#include "computations.h"
#include "nRFCalls.h"

#define EZI2CBUFFERSIZE 5
#define DEBUG_ENABLED 0
#define EFFICIENT_FILTER 0
#define DATA_DUMP_ENABLED 0
#define EZI2C_ENABLED 0
#define DELTA_T_INVERSE 1
#define X_REPORT_ENABLED 0
#define GYRO_REPORT_ENABLED 1


#define ADC_CHANNEL_COUNT 4
#define BUFFER_SIZE_PER_CHANNEL 32 
#define DMA_TD_LENGTH (2*BUFFER_SIZE_PER_CHANNEL*ADC_CHANNEL_COUNT)
#define FILTER_SIZE 32

#define ADC_SAMPLER_PERIOD 499
#define ADC_SAMPLER_COMPARE 250

/* DMA Configuration for DMA */
#define DMA_BYTES_PER_BURST 2
#define DMA_REQUEST_PER_BURST 1
#define DMA_SRC_BASE (CYDEV_PERIPH_BASE)
#define DMA_DST_BASE (CYDEV_SRAM_BASE)

uint16 ezi2cSource[EZI2CBUFFERSIZE];
uint8 channelNumber = 0;
uint16 time = 0;

void myi2c_init();
void adcInit();
void initAll(void);
void InitializeDma(void);
void initializeAdcHardware(void);
void sampleAdc(void);
int TX_PL_Write( uint8 *Data, int Data_Len);
void blink(uint8 repetition);
void performFiltering(void);
void squareMouse(void);
void transmit(uint8 *Data, int Data_Len);
void integrateToVelocity(void);
void integrateToDisplacement(void);
void computeDcOffset(void);
void performInitialFiltering(void);
void restartSampling(void);
void findIfStatic(void);
int16 scaleAndMap(int16 diffInDisp);
void enableTransmission(void);
int16 scaleAndMapGyro(void);
void applyHighPassFilter(void);

static int8 MouseData[4] = {0, 0, 0, 0}; 

uint16 xRawAcceleration = 0;
uint16 yRawAcceleration = 0;
uint16 zRawAcceleration = 0;
uint16 gyroRawValue = 0;

uint16 xFilteredAccelerationBuffer[64] = {0};
uint16 yFilteredAccelerationBuffer[64] = {0};
uint16 zFilteredAccelerationBuffer[64] = {0};
uint16 gyroFilteredBuffer[64] = {0};

uint32 xFiltered = 0;
uint32 yFiltered = 0;
uint32 zFiltered = 0;
uint32 gyroFiltered = 0;

uint8 newestFilteredDataIndex = 0;
uint8 oldestFilteredDataIndex = 1;

uint8 newestAdcSampleIndex = BUFFER_SIZE_PER_CHANNEL - 1;
uint8 oldestAdcSampleIndex = 0;

static uint16 myDelay = 0;

uint8 peformFilterFlag = 0;
uint8 initiateTransmissionFlag = 0;
 
int32 xVelocity = 0;
int32 yVelocity = 0;
int32 zVelocity = 0;

int32 gyroRotationAngle = 0;

int32 xDisplacementNew = 0;
int32 yDisplacementNew = 0;
int32 zDisplacementNew = 0;

int32 xDisplacementOld = 0;
int32 yDisplacementOld = 0;
int32 zDisplacementOld = 0;
int32 gyroRotationNew = 0;
int32 gyroRotationOld = 0;


uint16 xDcOffset = 0;
uint16 yDcOffset = 0;
uint16 zDcOffset = 0;
uint16 gyroDcOffset = 0;

uint8 isStatic = 0x01;
uint8 epsilon = 16;

/* Variable declarations for DMA */
/* Move these variable declarations to the top of the function */
uint8 DMA_Chan;
uint8 DMA_TD[1];
uint16 ADC_Samples[BUFFER_SIZE_PER_CHANNEL*ADC_CHANNEL_COUNT]; /* An array of length 32 with each containing three 16-bit words */ 

int8 mouseDataDump[512];
uint16 dataDumpIndex = 0;

int main()
{
    uint8 status;
    char* helloString = "hello";
    uint8 i=0;
    uint8 datacounter =0;
    
    initAll();
    
    CE_LOW();
    status = SPI_Send_command_without_ADDR(FLUSH_TX, NOP);
    
    #if DEBUG_ENABLED
    LCD_Position(0,0);
    LCD_PrintNumber(status);
    #endif
    
    
    /* 
    Start ADC sampling.
    Compute DC Offset using the first 32 ADC samples.
    Stop computation and reset everything (buffer pointers, PWM etc.).
    the xFiltered, yFiltered, zFiltered and gyroFiltered now has initial filtered value
    */
    performInitialFiltering();
    restartSampling();
    enableTransmission();
    
    for(;;)
    {
        if(peformFilterFlag == 1)
        {
            #if EZI2C_ENABLED
            EZI2C_1_EnableInt();
            #endif
            
            applyHighPassFilter();
            performFiltering();
            
            #if EZI2C_ENABLED
            EZI2C_1_DisableInt();
            #endif
            
            peformFilterFlag = 0;
            isr_triggerFiltering_ClearPending();
            
            findIfStatic();
            integrateToVelocity();
            integrateToDisplacement();
            //MouseData[1] = (int8)scaleAndMap(-xDisplacementOld+xDisplacementNew);
        
            #if DATA_DUMP_ENABLED
            dataDumpIndex++;
            dataDumpIndex = dataDumpIndex%512;
                
            #endif  
            
            if(isStatic == 0x01)
            {
                computeDcOffset();   
            }
            
            #if DATA_DUMP_ENABLED
            else
            {
                mouseDataDump[dataDumpIndex] =  MouseData[1];   
            }
            #endif
            
              
            
        }
           
        
        
        #if DEBUG_ENABLED
        LCD_Position(0,0);
        LCD_PrintString("Transmitting byte");
        #endif
        
        //////squareMouse();
               
        if(initiateTransmissionFlag == 1)
        {
        initiateTransmissionFlag = 0;
        isr_transmission_control_ClearPending();
        
        #if X_REPORT_ENABLED
        MouseData[1] = (int8)scaleAndMap(-xDisplacementOld+xDisplacementNew);
        #endif
        
        #if GYRO_REPORT_ENABLED
        if(Pin_Left_Click_Read() == 0x00)
            {
            MouseData[3] = (int8)scaleAndMapGyro();
            }
        else
            {
            MouseData[3] = 0;   
            }
        #endif    
            
        xDisplacementOld = xDisplacementNew; 
        gyroRotationOld = gyroRotationNew;
        transmit((uint8*)MouseData,4);       
        }
        
    }
}



void initAll(void){
    CyGlobalIntEnable; 
    PWM_heartBeat_Start();
   
    #if EZI2C_ENABLED 
    myi2c_init();
    #endif
    
    //adcInit();
    initializeAdcHardware();
   // channelSelectMux_Start();
    PWM_ADC_Sampler_WritePeriod(ADC_SAMPLER_PERIOD);
    PWM_ADC_Sampler_WriteCompare(ADC_SAMPLER_COMPARE);
    SPIM_NRF_Controller_Start();
    nRFTXSetting();
    
    #if DEBUG_ENABLED
    LCD_Start();
    #endif    
  //  UART_1_Start();
}

void InitializeDma(void)
{
	DMA_Chan = DMA_DmaInitialize(DMA_BYTES_PER_BURST, DMA_REQUEST_PER_BURST, 
    HI16(DMA_SRC_BASE), HI16(DMA_DST_BASE));
	DMA_TD[0] = CyDmaTdAllocate();
	CyDmaTdSetConfiguration(DMA_TD[0], DMA_TD_LENGTH, DMA_TD[0], TD_SWAP_EN | TD_INC_DST_ADR);
	CyDmaTdSetAddress(DMA_TD[0], LO16((uint32)ADC_DEC_SAMP_PTR), LO16((uint32)ADC_Samples));
	CyDmaChSetInitialTd(DMA_Chan, DMA_TD[0]);
	CyDmaChEnable(DMA_Chan, 1);
}



void initializeAdcHardware(void)
{
	InitializeDma();
	ADC_Start();
    isr_triggerFiltering_Start();
}	

void myi2c_init(){
    EZI2C_1_Start();
    EZI2C_1_EnableInt();
    EZI2C_1_SetAddress1(0x08);
    EZI2C_1_SetBuffer1(2*EZI2CBUFFERSIZE,0,(void *)ezi2cSource);
}

void adcInit(){
    ADC_Start();
    ADC_StartConvert(); 
}

void restartSampling(void)
{
    Control_Reg_PWM_Reset_Write(0x00); 
    PWM_ADC_Sampler_Start();     
}


void enableTransmission()
{
    Control_Reg_Start_Transmission_Write(0x00);
    PWM_Transmission_Control_Start();
    isr_transmission_control_Start();
}

void integrateToVelocity(void)
{
    if(isStatic == 0x00)
    {
//        if(xVelocity == 0)
//        {
//            xDcOffset = xFilteredAccelerationBuffer[30];
//        }
        xVelocity = xVelocity + (int32)((xFilteredAccelerationBuffer[31] - xDcOffset))/DELTA_T_INVERSE;   
        gyroRotationNew = gyroRotationNew + (int32) (gyroFilteredBuffer[31] - xDcOffset);
    }
    else
        xVelocity = 0;
}

void integrateToDisplacement(void)
{
    
    xDisplacementOld = xDisplacementNew;
    
    if(isStatic == 0x00)
    {     
        xDisplacementNew = xDisplacementNew + xVelocity/DELTA_T_INVERSE;
    }
    
    else
    {
        //xDisplacementNew = 0;
        //xDisplacementOld = 0;
    }
}

int16 scaleAndMap(int16 diffInDisp)
{
    if(abs(diffInDisp)<50)
    {
        return (diffInDisp/12);
    }
    
    else if(abs(diffInDisp)<800)
    {
        return (diffInDisp/160);
    }
    
    else if(abs(diffInDisp)<8000)
    {
        return (diffInDisp/800);
    }
    
    else if(abs(diffInDisp)<80000)
    {
        return (diffInDisp/5000);
    }
    
    else
    {
        return (abs(diffInDisp)/12500);
    }
    
//   if(abs(diffInDisp)<10)
//    {
//        if(diffInDisp > 0)
//        {
//            return 0;
//        }
//        
//        else 
//        {
//            return 0;
//        }
//    }
//    
//    else if(abs(diffInDisp)<127)
//    {
//        return (diffInDisp/2);
//    }
//    
//    else
//    {
//        if(diffInDisp > 127)
//        {
//            return 63;
//        }
//        
//        else 
//        {
//            return -63;
//        }
//    }

}

int16 scaleAndMapGyro(void)
{
    int16 gyroReportValue = 0; // = gyroRotationNew - gyroRotationOld;
    
    if(newestFilteredDataIndex == 0)
    {
        gyroReportValue = gyroFilteredBuffer[63] - gyroDcOffset;        
    }
    else
    {
        gyroReportValue = gyroFilteredBuffer[newestFilteredDataIndex - 1] - gyroDcOffset;
    }
    
   
//    if(gyroReportValue>4000)
//        {
//            gyroReportValue = 4000;
//        }
//    else if(gyroReportValue < -4000)
//        {
//            gyroReportValue = -4000;
//        }
//   
    
    if(abs(gyroReportValue) < 500)
    {
        gyroReportValue = gyroReportValue/40;
    }
    
    else if(abs(gyroReportValue) < 1000)
    {
        gyroReportValue = gyroReportValue/25;
    }
    
    else
    {
        gyroReportValue = 0;  
    }
    
    return (gyroReportValue);
}


void findIfStatic(void)
{
    uint8 staticCounter = 0;
    int16 diff = 0;
    
    diff = (int16)(xFilteredAccelerationBuffer[15] - xFilteredAccelerationBuffer[0]);
    if((abs(diff)<epsilon))
    {
         staticCounter++;
    }
    
    diff = (int16)(xFilteredAccelerationBuffer[31] - xFilteredAccelerationBuffer[15]);
    if((abs(diff)<epsilon))
    {
         staticCounter++;
    }
    
    diff = (int16)(xFilteredAccelerationBuffer[47] - xFilteredAccelerationBuffer[31]);
    if((abs(diff)<epsilon))
    {
         staticCounter++;
    }
    
    diff = (int16)(xFilteredAccelerationBuffer[63] - xFilteredAccelerationBuffer[47]);
    if((abs(diff)<epsilon))
    {
         staticCounter++;
    }
    
//    diff = (int16)(ADC_Samples[28] - ADC_Samples[0]);
//    if((abs(diff)<epsilon))
//    {
//         staticCounter++;
//    }
//    
//    diff = (int16)(ADC_Samples[60] - ADC_Samples[28]);
//    if((abs(diff)<epsilon))
//    {
//         staticCounter++;
//    }
//    
//    diff = (int16)(ADC_Samples[92] - ADC_Samples[60]);
//    if((abs(diff)<epsilon))
//    {
//         staticCounter++;
//    }
//    
//    diff = (int16)(ADC_Samples[124] - ADC_Samples[92]);
//    if((abs(diff)<epsilon))
//    {
//         staticCounter++;
//    }
    
    if(staticCounter == 4)
    {
        isStatic = 0x01;   
    }
    
    else
    {
        isStatic = 0x00;   
    }
    
    
}


void performFiltering(void)
{
    char i;
    uint16 temp = 0;
    
    #if (EFFICIENT_FILTER == 0)
    
    xFiltered = 0;
    yFiltered = 0;
    zFiltered = 0;
    gyroFiltered = 0;
    
    for(i=0;i<FILTER_SIZE;i++)
        {
            xFiltered += ADC_Samples[4*i];    
            yFiltered += ADC_Samples[4*i+1];
            zFiltered += ADC_Samples[4*i+2];
            gyroFiltered += ADC_Samples[4*i+3];
        }
        
        newestAdcSampleIndex++;
        newestAdcSampleIndex =  newestAdcSampleIndex%BUFFER_SIZE_PER_CHANNEL;
        
        oldestAdcSampleIndex++;
        oldestAdcSampleIndex = oldestAdcSampleIndex%BUFFER_SIZE_PER_CHANNEL;
    
    #endif    
    
    #if EFFICIENT_FILTER
        newestAdcSampleIndex++;
        newestAdcSampleIndex =  newestAdcSampleIndex%BUFFER_SIZE_PER_CHANNEL;
        
        oldestAdcSampleIndex++;
        oldestAdcSampleIndex = oldestAdcSampleIndex%BUFFER_SIZE_PER_CHANNEL;
    
        xFiltered = xFiltered + (uint32)ADC_Samples[4*newestAdcSampleIndex];
        yFiltered = yFiltered + (uint32)ADC_Samples[4*newestAdcSampleIndex + 1];
        zFiltered = zFiltered + (uint32)ADC_Samples[4*newestAdcSampleIndex + 2];
        gyroFiltered = gyroFiltered + (uint32)ADC_Samples[4*newestAdcSampleIndex + 3];
    
    #endif
          
    
        xFilteredAccelerationBuffer[newestFilteredDataIndex] = (uint16) (xFiltered>>5);
        
        #if EZI2C_ENABLED       
        ezi2cSource[0] = ADC_Samples[4*newestAdcSampleIndex];
        #endif
        
        yFilteredAccelerationBuffer[newestFilteredDataIndex] = (uint16) (yFiltered>>5);
        
        #if EZI2C_ENABLED 
        ezi2cSource[1] = ADC_Samples[4*newestAdcSampleIndex+1];
        #endif
        
        zFilteredAccelerationBuffer[newestFilteredDataIndex] = (uint16) (zFiltered>>5);
        
        #if EZI2C_ENABLED 
        ezi2cSource[2] = ADC_Samples[4*newestAdcSampleIndex+2];
        #endif
        
        gyroFilteredBuffer[newestFilteredDataIndex] = (uint16) (gyroFiltered>>5);
        
        #if EZI2C_ENABLED 
        ezi2cSource[3] = ADC_Samples[4*newestAdcSampleIndex+3];

//        time++;
//        ezi2cSource[4] = time;
        #endif
        
        #if EFFICIENT_FILTER
        xFiltered = xFiltered - (uint32)ADC_Samples[4*oldestAdcSampleIndex];
        yFiltered = yFiltered - (uint32)ADC_Samples[4*oldestAdcSampleIndex + 1];
        zFiltered = zFiltered - (uint32)ADC_Samples[4*oldestAdcSampleIndex + 2];
        gyroFiltered = gyroFiltered - (uint32)ADC_Samples[4*oldestAdcSampleIndex + 3];
        #endif
        
        newestFilteredDataIndex++;
        newestFilteredDataIndex =  newestFilteredDataIndex%64;
        
        oldestFilteredDataIndex++;
        oldestFilteredDataIndex = oldestFilteredDataIndex%64;
        
}

void applyHighPassFilter(void)
{
    int k;
    
    for(k=1;k<63;k++)
    {
        gyroFilteredBuffer[k] = gyroFilteredBuffer[k] - gyroFilteredBuffer[k-1] + (9*gyroFilteredBuffer[k-1])/10;
    }
}

void performInitialFiltering(void)
{
    uint8 sampleCount = 0;
    Control_Reg_PWM_Reset_Write(0x00);
    PWM_ADC_Sampler_Start();
    
    
    while(sampleCount<64)
    {
       
        
        if(peformFilterFlag == 1)
        {
                      
            computeDcOffset();
            peformFilterFlag = 0;
            isr_triggerFiltering_ClearPending();
            sampleCount++;
            
            if(sampleCount == 63)
            {
                PWM_ADC_Sampler_Stop();
                Control_Reg_PWM_Reset_Write(0x01);
                newestAdcSampleIndex = BUFFER_SIZE_PER_CHANNEL - 1;
                oldestAdcSampleIndex = 0;
                
                /*temporary fix for infinite loop condition*/
                sampleCount++;
                
                xFiltered = xFiltered - xDcOffset;
                yFiltered = yFiltered - yDcOffset;
                zFiltered = zFiltered - zDcOffset;
                gyroFiltered = gyroFiltered - gyroDcOffset;
                
            }
        }
       
    }   
}


void computeDcOffset(void)
{
    int i;
    uint16 maxX = 0;
    
    xFiltered = 0;
    yFiltered = 0;
    zFiltered = 0;
    gyroFiltered = 0;
    
    for(i=0;i<FILTER_SIZE;i++)
        {
            xFiltered += ADC_Samples[4*i];    
            yFiltered += ADC_Samples[4*i+1];
            zFiltered += ADC_Samples[4*i+2];
            gyroFiltered += ADC_Samples[4*i+3];
            
//            if(maxX < abs(ADC_Samples[4*i] - xDcOffset))
//            {
//                maxX = abs(ADC_Samples[4*i] - xDcOffset);
//                epsilon = maxX+5;
//            }
        }
        
        xDcOffset = (uint16) (xFiltered>>5); 
        yDcOffset = (uint16) (yFiltered>>5);
        zDcOffset = (uint16) (zFiltered>>5);
        gyroDcOffset = (uint16) (gyroFiltered>>5);
}


void squareMouse(void)
{
   
    switch (myDelay){									/* Switch statement creates a state machine for mouse movement */
			case 128:	MouseData[1] = 5;  			/* Moves mouse to right */
						MouseData[2] = 0;  			/* Keeps Y static */
					//	LCD_Char_1_Position(1,0);
					//	LCD_Char_1_PrintString("Mouse Right");
						blink(0);
                        break;
			
			case 256:	MouseData[1] = 0; 				/* Keeps X static */
						MouseData[2] = 5; 				/* Moves mouse down */
					//	LCD_Char_1_Position(1,0);
					//	LCD_Char_1_PrintString("Mouse Down ");
						blink(1);
                        break;
			
			case 384:	MouseData[1] = -5; 			/* Moves mouse to left */
						MouseData[2] = 0;  			/* Keeps Y static */
					//	LCD_Char_1_Position(1,0);
					//	LCD_Char_1_PrintString("Mouse Left ");
						blink(2);
                        break;
			
			case 512:	MouseData[1] = 0;  			/* Keeps X static */
						MouseData[2] = -5; 			/* Moves mouse up */
					//	LCD_Char_1_Position(1,0);
					//	LCD_Char_1_PrintString("Mouse Up   ");
						blink(3);
                        myDelay=0;
						break;  
			default:	break;  
    }
					
	myDelay++;
    
}


void transmit(uint8 *Data, int Data_Len)
    {
        uint8 status;
        uint8 status_temp;
        
        TX_PL_Write((uint8*)Data,Data_Len);
        
        CE_HIGH();
        CE_LOW();
    
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        while ((status  & TX_DS) != TX_DS ){
        if ((status & MAX_RT) != 0) {

        // Clear MAX_RT bit in status register
        status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|MAX_RT));
        #if DEBUG_ENABLED
        LCD_Position(0,0);
        LCD_PrintString("Packet lost");//Toggle LED1 to indicate that a packet is lost during Transmission
        blink(1);
        #endif 
        // No communication event here
        // ....
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        break;
        }
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        }

        if ((status & TX_DS) != 0) {//One packet of data sent successfully
        // Clear TX_DS bit in status register 
        status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|TX_DS));
        #if DEBUG_ENABLED
        LCD_Position(0,0);
        LCD_PrintString("1 Packet sent");
        blink(0);
        #endif
        }
        
        if ((status & MAX_RT) != 0) 
        {
        // Clear TX_DS bit in status register
        status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|MAX_RT));
        #if DEBUG_ENABLED
        LCD_Position(0,0);
        LCD_PrintString("MAX_RT is cleared");
        blink(3);
        #endif
        }

        if ((status & TX_FULL) != 0)  {
          // Flush TX FIFO (in TX mode          
          status_temp = SPI_Send_command_without_ADDR(FLUSH_TX, NOP);
        }
       status = SPI_Send_command_without_ADDR(NOP,NOP);
        
    }


/* [] END OF FILE */
