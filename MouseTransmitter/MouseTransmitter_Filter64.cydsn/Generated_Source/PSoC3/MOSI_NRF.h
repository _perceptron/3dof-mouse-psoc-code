/*******************************************************************************
* File Name: MOSI_NRF.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_MOSI_NRF_H) /* Pins MOSI_NRF_H */
#define CY_PINS_MOSI_NRF_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "MOSI_NRF_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    MOSI_NRF_Write(uint8 value) ;
void    MOSI_NRF_SetDriveMode(uint8 mode) ;
uint8   MOSI_NRF_ReadDataReg(void) ;
uint8   MOSI_NRF_Read(void) ;
uint8   MOSI_NRF_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define MOSI_NRF_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define MOSI_NRF_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define MOSI_NRF_DM_RES_UP          PIN_DM_RES_UP
#define MOSI_NRF_DM_RES_DWN         PIN_DM_RES_DWN
#define MOSI_NRF_DM_OD_LO           PIN_DM_OD_LO
#define MOSI_NRF_DM_OD_HI           PIN_DM_OD_HI
#define MOSI_NRF_DM_STRONG          PIN_DM_STRONG
#define MOSI_NRF_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define MOSI_NRF_MASK               MOSI_NRF__MASK
#define MOSI_NRF_SHIFT              MOSI_NRF__SHIFT
#define MOSI_NRF_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define MOSI_NRF_PS                     (* (reg8 *) MOSI_NRF__PS)
/* Data Register */
#define MOSI_NRF_DR                     (* (reg8 *) MOSI_NRF__DR)
/* Port Number */
#define MOSI_NRF_PRT_NUM                (* (reg8 *) MOSI_NRF__PRT) 
/* Connect to Analog Globals */                                                  
#define MOSI_NRF_AG                     (* (reg8 *) MOSI_NRF__AG)                       
/* Analog MUX bux enable */
#define MOSI_NRF_AMUX                   (* (reg8 *) MOSI_NRF__AMUX) 
/* Bidirectional Enable */                                                        
#define MOSI_NRF_BIE                    (* (reg8 *) MOSI_NRF__BIE)
/* Bit-mask for Aliased Register Access */
#define MOSI_NRF_BIT_MASK               (* (reg8 *) MOSI_NRF__BIT_MASK)
/* Bypass Enable */
#define MOSI_NRF_BYP                    (* (reg8 *) MOSI_NRF__BYP)
/* Port wide control signals */                                                   
#define MOSI_NRF_CTL                    (* (reg8 *) MOSI_NRF__CTL)
/* Drive Modes */
#define MOSI_NRF_DM0                    (* (reg8 *) MOSI_NRF__DM0) 
#define MOSI_NRF_DM1                    (* (reg8 *) MOSI_NRF__DM1)
#define MOSI_NRF_DM2                    (* (reg8 *) MOSI_NRF__DM2) 
/* Input Buffer Disable Override */
#define MOSI_NRF_INP_DIS                (* (reg8 *) MOSI_NRF__INP_DIS)
/* LCD Common or Segment Drive */
#define MOSI_NRF_LCD_COM_SEG            (* (reg8 *) MOSI_NRF__LCD_COM_SEG)
/* Enable Segment LCD */
#define MOSI_NRF_LCD_EN                 (* (reg8 *) MOSI_NRF__LCD_EN)
/* Slew Rate Control */
#define MOSI_NRF_SLW                    (* (reg8 *) MOSI_NRF__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define MOSI_NRF_PRTDSI__CAPS_SEL       (* (reg8 *) MOSI_NRF__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define MOSI_NRF_PRTDSI__DBL_SYNC_IN    (* (reg8 *) MOSI_NRF__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define MOSI_NRF_PRTDSI__OE_SEL0        (* (reg8 *) MOSI_NRF__PRTDSI__OE_SEL0) 
#define MOSI_NRF_PRTDSI__OE_SEL1        (* (reg8 *) MOSI_NRF__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define MOSI_NRF_PRTDSI__OUT_SEL0       (* (reg8 *) MOSI_NRF__PRTDSI__OUT_SEL0) 
#define MOSI_NRF_PRTDSI__OUT_SEL1       (* (reg8 *) MOSI_NRF__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define MOSI_NRF_PRTDSI__SYNC_OUT       (* (reg8 *) MOSI_NRF__PRTDSI__SYNC_OUT) 


#if defined(MOSI_NRF__INTSTAT)  /* Interrupt Registers */

    #define MOSI_NRF_INTSTAT                (* (reg8 *) MOSI_NRF__INTSTAT)
    #define MOSI_NRF_SNAP                   (* (reg8 *) MOSI_NRF__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins MOSI_NRF_H */


/* [] END OF FILE */
