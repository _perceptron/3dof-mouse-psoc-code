/*******************************************************************************
* File Name: NRF_CSN.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_NRF_CSN_H) /* Pins NRF_CSN_H */
#define CY_PINS_NRF_CSN_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "NRF_CSN_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    NRF_CSN_Write(uint8 value) ;
void    NRF_CSN_SetDriveMode(uint8 mode) ;
uint8   NRF_CSN_ReadDataReg(void) ;
uint8   NRF_CSN_Read(void) ;
uint8   NRF_CSN_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define NRF_CSN_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define NRF_CSN_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define NRF_CSN_DM_RES_UP          PIN_DM_RES_UP
#define NRF_CSN_DM_RES_DWN         PIN_DM_RES_DWN
#define NRF_CSN_DM_OD_LO           PIN_DM_OD_LO
#define NRF_CSN_DM_OD_HI           PIN_DM_OD_HI
#define NRF_CSN_DM_STRONG          PIN_DM_STRONG
#define NRF_CSN_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define NRF_CSN_MASK               NRF_CSN__MASK
#define NRF_CSN_SHIFT              NRF_CSN__SHIFT
#define NRF_CSN_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define NRF_CSN_PS                     (* (reg8 *) NRF_CSN__PS)
/* Data Register */
#define NRF_CSN_DR                     (* (reg8 *) NRF_CSN__DR)
/* Port Number */
#define NRF_CSN_PRT_NUM                (* (reg8 *) NRF_CSN__PRT) 
/* Connect to Analog Globals */                                                  
#define NRF_CSN_AG                     (* (reg8 *) NRF_CSN__AG)                       
/* Analog MUX bux enable */
#define NRF_CSN_AMUX                   (* (reg8 *) NRF_CSN__AMUX) 
/* Bidirectional Enable */                                                        
#define NRF_CSN_BIE                    (* (reg8 *) NRF_CSN__BIE)
/* Bit-mask for Aliased Register Access */
#define NRF_CSN_BIT_MASK               (* (reg8 *) NRF_CSN__BIT_MASK)
/* Bypass Enable */
#define NRF_CSN_BYP                    (* (reg8 *) NRF_CSN__BYP)
/* Port wide control signals */                                                   
#define NRF_CSN_CTL                    (* (reg8 *) NRF_CSN__CTL)
/* Drive Modes */
#define NRF_CSN_DM0                    (* (reg8 *) NRF_CSN__DM0) 
#define NRF_CSN_DM1                    (* (reg8 *) NRF_CSN__DM1)
#define NRF_CSN_DM2                    (* (reg8 *) NRF_CSN__DM2) 
/* Input Buffer Disable Override */
#define NRF_CSN_INP_DIS                (* (reg8 *) NRF_CSN__INP_DIS)
/* LCD Common or Segment Drive */
#define NRF_CSN_LCD_COM_SEG            (* (reg8 *) NRF_CSN__LCD_COM_SEG)
/* Enable Segment LCD */
#define NRF_CSN_LCD_EN                 (* (reg8 *) NRF_CSN__LCD_EN)
/* Slew Rate Control */
#define NRF_CSN_SLW                    (* (reg8 *) NRF_CSN__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define NRF_CSN_PRTDSI__CAPS_SEL       (* (reg8 *) NRF_CSN__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define NRF_CSN_PRTDSI__DBL_SYNC_IN    (* (reg8 *) NRF_CSN__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define NRF_CSN_PRTDSI__OE_SEL0        (* (reg8 *) NRF_CSN__PRTDSI__OE_SEL0) 
#define NRF_CSN_PRTDSI__OE_SEL1        (* (reg8 *) NRF_CSN__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define NRF_CSN_PRTDSI__OUT_SEL0       (* (reg8 *) NRF_CSN__PRTDSI__OUT_SEL0) 
#define NRF_CSN_PRTDSI__OUT_SEL1       (* (reg8 *) NRF_CSN__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define NRF_CSN_PRTDSI__SYNC_OUT       (* (reg8 *) NRF_CSN__PRTDSI__SYNC_OUT) 


#if defined(NRF_CSN__INTSTAT)  /* Interrupt Registers */

    #define NRF_CSN_INTSTAT                (* (reg8 *) NRF_CSN__INTSTAT)
    #define NRF_CSN_SNAP                   (* (reg8 *) NRF_CSN__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins NRF_CSN_H */


/* [] END OF FILE */
