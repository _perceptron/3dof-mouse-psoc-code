/*******************************************************************************
* File Name: NRF_MOSI.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "NRF_MOSI.h"


/*******************************************************************************
* Function Name: NRF_MOSI_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void NRF_MOSI_Write(uint8 value) 
{
    uint8 staticBits = (NRF_MOSI_DR & (uint8)(~NRF_MOSI_MASK));
    NRF_MOSI_DR = staticBits | ((uint8)(value << NRF_MOSI_SHIFT) & NRF_MOSI_MASK);
}


/*******************************************************************************
* Function Name: NRF_MOSI_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void NRF_MOSI_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(NRF_MOSI_0, mode);
}


/*******************************************************************************
* Function Name: NRF_MOSI_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro NRF_MOSI_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 NRF_MOSI_Read(void) 
{
    return (NRF_MOSI_PS & NRF_MOSI_MASK) >> NRF_MOSI_SHIFT;
}


/*******************************************************************************
* Function Name: NRF_MOSI_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 NRF_MOSI_ReadDataReg(void) 
{
    return (NRF_MOSI_DR & NRF_MOSI_MASK) >> NRF_MOSI_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(NRF_MOSI_INTSTAT) 

    /*******************************************************************************
    * Function Name: NRF_MOSI_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 NRF_MOSI_ClearInterrupt(void) 
    {
        return (NRF_MOSI_INTSTAT & NRF_MOSI_MASK) >> NRF_MOSI_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
