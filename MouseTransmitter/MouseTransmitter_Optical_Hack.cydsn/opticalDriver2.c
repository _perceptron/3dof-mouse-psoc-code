/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "opticalDriver2.h"
#include "SPIM_opticalDriver_2.h"
#include "cytypes.h"
#include "opticalDriver_NCS_2.h"
#include "opticalDriver_Reset_2.h"


int dumpWidth_2 = 256; // Number of pixels to read for each frame.
uint8 frame_2[256];
uint8 productId_2;
uint8 revisionId_2;

// Chip Select -> high
void NCS_HIGH_2  (void){
    opticalDriver_NCS_2_Write(0x01);    
}
// Chip Select -> low
void NCS_LOW_2 (void){
    opticalDriver_NCS_2_Write(0x00);
}

uint8 SPI_opticalDriver_2_SendByte(uint8 spiData){

    uint8 rxStatus2 = 0x00;
    SPIM_opticalDriver_2_WriteTxData(spiData);
    rxStatus2 = SPIM_opticalDriver_2_RX_STATUS_REG;
    while((rxStatus2&SPIM_opticalDriver_2_STS_RX_FIFO_NOT_EMPTY) != SPIM_opticalDriver_2_STS_RX_FIFO_NOT_EMPTY){
        rxStatus2 = SPIM_opticalDriver_2_RX_STATUS_REG;
    }
    return (uint8)SPIM_opticalDriver_2_ReadRxData()  ;

}


void opticalDriverWriteRegister_2(uint8 address, uint8 regData) {
    uint8 status2;
    address |= 0x80; // MSB indicates write mode.
  
    NCS_LOW_2();
    status2 = SPI_opticalDriver_2_SendByte(address);
    CyDelayUs(40);
    status2 = SPI_opticalDriver_2_SendByte(regData);
    NCS_HIGH_2();
    CyDelayUs(40); // tSWW = 30us , tSWR = 20us min.
}


uint8 opticalDriverReadRegister_2(uint8 address) {

    uint8 status2;
    
    NCS_LOW_2();
    status2 = SPI_opticalDriver_2_SendByte(address);
    CyDelayUs(10); //tSRAD = 4us min.
    status2 = SPI_opticalDriver_2_SendByte(0x00); //dummy write to pump data out of the sensor
    NCS_HIGH_2();
    CyDelayUs(2); // tSRR, tSRR = 250ns min.  
  
    return status2;
}


void reset_2() {
    //opticalDriverWriteRegister(REG_CHIP_RESET,0x5A); //Write 0x5a to initiate chip RESET.
    opticalDriver_Reset_2_Write(0);
    CyDelayUs(20);
    opticalDriver_Reset_2_Write(1);
}

void setupOpticalSensor_2() {
  //Serial.begin(115200);
    SPIM_opticalDriver_2_Start();
    reset_2();
    productId_2 = opticalDriverReadRegister_2(REG_PRODUCT_ID);
    revisionId_2 = opticalDriverReadRegister_2(REG_REVISION_ID);
    opticalDriverWriteRegister_2(0x0d,0x01);
}

uint8 opticalDriverGetDeltaX_2(){
    return opticalDriverReadRegister_2(REG_DELTA_X);
}

uint8 opticalDriverGetDeltaY_2(){
    return opticalDriverReadRegister_2(REG_DELTA_Y);
}

uint8 opticalDriverGetProductId_2(void)
{
    return opticalDriverReadRegister_2(REG_PRODUCT_ID);
}

uint8 opticalDriverIsMouseMoving_2(void)
{
    uint8 status2 = opticalDriverReadRegister_2(REG_MOTION);
    
    if((status2&0x80) == 0x80)
    {
        return MOUSE_MOVING;
    }
    
    else
    return MOUSE_NOT_MOVING;
}

/* [] END OF FILE */
