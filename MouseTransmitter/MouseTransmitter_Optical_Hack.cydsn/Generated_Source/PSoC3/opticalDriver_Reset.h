/*******************************************************************************
* File Name: opticalDriver_Reset.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_opticalDriver_Reset_H) /* Pins opticalDriver_Reset_H */
#define CY_PINS_opticalDriver_Reset_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "opticalDriver_Reset_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    opticalDriver_Reset_Write(uint8 value) ;
void    opticalDriver_Reset_SetDriveMode(uint8 mode) ;
uint8   opticalDriver_Reset_ReadDataReg(void) ;
uint8   opticalDriver_Reset_Read(void) ;
uint8   opticalDriver_Reset_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define opticalDriver_Reset_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define opticalDriver_Reset_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define opticalDriver_Reset_DM_RES_UP          PIN_DM_RES_UP
#define opticalDriver_Reset_DM_RES_DWN         PIN_DM_RES_DWN
#define opticalDriver_Reset_DM_OD_LO           PIN_DM_OD_LO
#define opticalDriver_Reset_DM_OD_HI           PIN_DM_OD_HI
#define opticalDriver_Reset_DM_STRONG          PIN_DM_STRONG
#define opticalDriver_Reset_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define opticalDriver_Reset_MASK               opticalDriver_Reset__MASK
#define opticalDriver_Reset_SHIFT              opticalDriver_Reset__SHIFT
#define opticalDriver_Reset_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define opticalDriver_Reset_PS                     (* (reg8 *) opticalDriver_Reset__PS)
/* Data Register */
#define opticalDriver_Reset_DR                     (* (reg8 *) opticalDriver_Reset__DR)
/* Port Number */
#define opticalDriver_Reset_PRT_NUM                (* (reg8 *) opticalDriver_Reset__PRT) 
/* Connect to Analog Globals */                                                  
#define opticalDriver_Reset_AG                     (* (reg8 *) opticalDriver_Reset__AG)                       
/* Analog MUX bux enable */
#define opticalDriver_Reset_AMUX                   (* (reg8 *) opticalDriver_Reset__AMUX) 
/* Bidirectional Enable */                                                        
#define opticalDriver_Reset_BIE                    (* (reg8 *) opticalDriver_Reset__BIE)
/* Bit-mask for Aliased Register Access */
#define opticalDriver_Reset_BIT_MASK               (* (reg8 *) opticalDriver_Reset__BIT_MASK)
/* Bypass Enable */
#define opticalDriver_Reset_BYP                    (* (reg8 *) opticalDriver_Reset__BYP)
/* Port wide control signals */                                                   
#define opticalDriver_Reset_CTL                    (* (reg8 *) opticalDriver_Reset__CTL)
/* Drive Modes */
#define opticalDriver_Reset_DM0                    (* (reg8 *) opticalDriver_Reset__DM0) 
#define opticalDriver_Reset_DM1                    (* (reg8 *) opticalDriver_Reset__DM1)
#define opticalDriver_Reset_DM2                    (* (reg8 *) opticalDriver_Reset__DM2) 
/* Input Buffer Disable Override */
#define opticalDriver_Reset_INP_DIS                (* (reg8 *) opticalDriver_Reset__INP_DIS)
/* LCD Common or Segment Drive */
#define opticalDriver_Reset_LCD_COM_SEG            (* (reg8 *) opticalDriver_Reset__LCD_COM_SEG)
/* Enable Segment LCD */
#define opticalDriver_Reset_LCD_EN                 (* (reg8 *) opticalDriver_Reset__LCD_EN)
/* Slew Rate Control */
#define opticalDriver_Reset_SLW                    (* (reg8 *) opticalDriver_Reset__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define opticalDriver_Reset_PRTDSI__CAPS_SEL       (* (reg8 *) opticalDriver_Reset__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define opticalDriver_Reset_PRTDSI__DBL_SYNC_IN    (* (reg8 *) opticalDriver_Reset__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define opticalDriver_Reset_PRTDSI__OE_SEL0        (* (reg8 *) opticalDriver_Reset__PRTDSI__OE_SEL0) 
#define opticalDriver_Reset_PRTDSI__OE_SEL1        (* (reg8 *) opticalDriver_Reset__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define opticalDriver_Reset_PRTDSI__OUT_SEL0       (* (reg8 *) opticalDriver_Reset__PRTDSI__OUT_SEL0) 
#define opticalDriver_Reset_PRTDSI__OUT_SEL1       (* (reg8 *) opticalDriver_Reset__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define opticalDriver_Reset_PRTDSI__SYNC_OUT       (* (reg8 *) opticalDriver_Reset__PRTDSI__SYNC_OUT) 


#if defined(opticalDriver_Reset__INTSTAT)  /* Interrupt Registers */

    #define opticalDriver_Reset_INTSTAT                (* (reg8 *) opticalDriver_Reset__INTSTAT)
    #define opticalDriver_Reset_SNAP                   (* (reg8 *) opticalDriver_Reset__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins opticalDriver_Reset_H */


/* [] END OF FILE */
