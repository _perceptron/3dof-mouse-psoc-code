/*******************************************************************************
* File Name: NRF_CE.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_NRF_CE_H) /* Pins NRF_CE_H */
#define CY_PINS_NRF_CE_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "NRF_CE_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    NRF_CE_Write(uint8 value) ;
void    NRF_CE_SetDriveMode(uint8 mode) ;
uint8   NRF_CE_ReadDataReg(void) ;
uint8   NRF_CE_Read(void) ;
uint8   NRF_CE_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define NRF_CE_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define NRF_CE_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define NRF_CE_DM_RES_UP          PIN_DM_RES_UP
#define NRF_CE_DM_RES_DWN         PIN_DM_RES_DWN
#define NRF_CE_DM_OD_LO           PIN_DM_OD_LO
#define NRF_CE_DM_OD_HI           PIN_DM_OD_HI
#define NRF_CE_DM_STRONG          PIN_DM_STRONG
#define NRF_CE_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define NRF_CE_MASK               NRF_CE__MASK
#define NRF_CE_SHIFT              NRF_CE__SHIFT
#define NRF_CE_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define NRF_CE_PS                     (* (reg8 *) NRF_CE__PS)
/* Data Register */
#define NRF_CE_DR                     (* (reg8 *) NRF_CE__DR)
/* Port Number */
#define NRF_CE_PRT_NUM                (* (reg8 *) NRF_CE__PRT) 
/* Connect to Analog Globals */                                                  
#define NRF_CE_AG                     (* (reg8 *) NRF_CE__AG)                       
/* Analog MUX bux enable */
#define NRF_CE_AMUX                   (* (reg8 *) NRF_CE__AMUX) 
/* Bidirectional Enable */                                                        
#define NRF_CE_BIE                    (* (reg8 *) NRF_CE__BIE)
/* Bit-mask for Aliased Register Access */
#define NRF_CE_BIT_MASK               (* (reg8 *) NRF_CE__BIT_MASK)
/* Bypass Enable */
#define NRF_CE_BYP                    (* (reg8 *) NRF_CE__BYP)
/* Port wide control signals */                                                   
#define NRF_CE_CTL                    (* (reg8 *) NRF_CE__CTL)
/* Drive Modes */
#define NRF_CE_DM0                    (* (reg8 *) NRF_CE__DM0) 
#define NRF_CE_DM1                    (* (reg8 *) NRF_CE__DM1)
#define NRF_CE_DM2                    (* (reg8 *) NRF_CE__DM2) 
/* Input Buffer Disable Override */
#define NRF_CE_INP_DIS                (* (reg8 *) NRF_CE__INP_DIS)
/* LCD Common or Segment Drive */
#define NRF_CE_LCD_COM_SEG            (* (reg8 *) NRF_CE__LCD_COM_SEG)
/* Enable Segment LCD */
#define NRF_CE_LCD_EN                 (* (reg8 *) NRF_CE__LCD_EN)
/* Slew Rate Control */
#define NRF_CE_SLW                    (* (reg8 *) NRF_CE__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define NRF_CE_PRTDSI__CAPS_SEL       (* (reg8 *) NRF_CE__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define NRF_CE_PRTDSI__DBL_SYNC_IN    (* (reg8 *) NRF_CE__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define NRF_CE_PRTDSI__OE_SEL0        (* (reg8 *) NRF_CE__PRTDSI__OE_SEL0) 
#define NRF_CE_PRTDSI__OE_SEL1        (* (reg8 *) NRF_CE__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define NRF_CE_PRTDSI__OUT_SEL0       (* (reg8 *) NRF_CE__PRTDSI__OUT_SEL0) 
#define NRF_CE_PRTDSI__OUT_SEL1       (* (reg8 *) NRF_CE__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define NRF_CE_PRTDSI__SYNC_OUT       (* (reg8 *) NRF_CE__PRTDSI__SYNC_OUT) 


#if defined(NRF_CE__INTSTAT)  /* Interrupt Registers */

    #define NRF_CE_INTSTAT                (* (reg8 *) NRF_CE__INTSTAT)
    #define NRF_CE_SNAP                   (* (reg8 *) NRF_CE__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins NRF_CE_H */


/* [] END OF FILE */
