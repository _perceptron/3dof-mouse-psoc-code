/*******************************************************************************
* File Name: NCS_opticalDirver.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "NCS_opticalDirver.h"


/*******************************************************************************
* Function Name: NCS_opticalDirver_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void NCS_opticalDirver_Write(uint8 value) 
{
    uint8 staticBits = (NCS_opticalDirver_DR & (uint8)(~NCS_opticalDirver_MASK));
    NCS_opticalDirver_DR = staticBits | ((uint8)(value << NCS_opticalDirver_SHIFT) & NCS_opticalDirver_MASK);
}


/*******************************************************************************
* Function Name: NCS_opticalDirver_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void NCS_opticalDirver_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(NCS_opticalDirver_0, mode);
}


/*******************************************************************************
* Function Name: NCS_opticalDirver_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro NCS_opticalDirver_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 NCS_opticalDirver_Read(void) 
{
    return (NCS_opticalDirver_PS & NCS_opticalDirver_MASK) >> NCS_opticalDirver_SHIFT;
}


/*******************************************************************************
* Function Name: NCS_opticalDirver_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 NCS_opticalDirver_ReadDataReg(void) 
{
    return (NCS_opticalDirver_DR & NCS_opticalDirver_MASK) >> NCS_opticalDirver_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(NCS_opticalDirver_INTSTAT) 

    /*******************************************************************************
    * Function Name: NCS_opticalDirver_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 NCS_opticalDirver_ClearInterrupt(void) 
    {
        return (NCS_opticalDirver_INTSTAT & NCS_opticalDirver_MASK) >> NCS_opticalDirver_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
