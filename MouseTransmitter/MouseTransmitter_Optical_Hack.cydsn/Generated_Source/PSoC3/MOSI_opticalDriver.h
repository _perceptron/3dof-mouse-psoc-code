/*******************************************************************************
* File Name: MOSI_opticalDriver.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_MOSI_opticalDriver_H) /* Pins MOSI_opticalDriver_H */
#define CY_PINS_MOSI_opticalDriver_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "MOSI_opticalDriver_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    MOSI_opticalDriver_Write(uint8 value) ;
void    MOSI_opticalDriver_SetDriveMode(uint8 mode) ;
uint8   MOSI_opticalDriver_ReadDataReg(void) ;
uint8   MOSI_opticalDriver_Read(void) ;
uint8   MOSI_opticalDriver_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define MOSI_opticalDriver_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define MOSI_opticalDriver_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define MOSI_opticalDriver_DM_RES_UP          PIN_DM_RES_UP
#define MOSI_opticalDriver_DM_RES_DWN         PIN_DM_RES_DWN
#define MOSI_opticalDriver_DM_OD_LO           PIN_DM_OD_LO
#define MOSI_opticalDriver_DM_OD_HI           PIN_DM_OD_HI
#define MOSI_opticalDriver_DM_STRONG          PIN_DM_STRONG
#define MOSI_opticalDriver_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define MOSI_opticalDriver_MASK               MOSI_opticalDriver__MASK
#define MOSI_opticalDriver_SHIFT              MOSI_opticalDriver__SHIFT
#define MOSI_opticalDriver_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define MOSI_opticalDriver_PS                     (* (reg8 *) MOSI_opticalDriver__PS)
/* Data Register */
#define MOSI_opticalDriver_DR                     (* (reg8 *) MOSI_opticalDriver__DR)
/* Port Number */
#define MOSI_opticalDriver_PRT_NUM                (* (reg8 *) MOSI_opticalDriver__PRT) 
/* Connect to Analog Globals */                                                  
#define MOSI_opticalDriver_AG                     (* (reg8 *) MOSI_opticalDriver__AG)                       
/* Analog MUX bux enable */
#define MOSI_opticalDriver_AMUX                   (* (reg8 *) MOSI_opticalDriver__AMUX) 
/* Bidirectional Enable */                                                        
#define MOSI_opticalDriver_BIE                    (* (reg8 *) MOSI_opticalDriver__BIE)
/* Bit-mask for Aliased Register Access */
#define MOSI_opticalDriver_BIT_MASK               (* (reg8 *) MOSI_opticalDriver__BIT_MASK)
/* Bypass Enable */
#define MOSI_opticalDriver_BYP                    (* (reg8 *) MOSI_opticalDriver__BYP)
/* Port wide control signals */                                                   
#define MOSI_opticalDriver_CTL                    (* (reg8 *) MOSI_opticalDriver__CTL)
/* Drive Modes */
#define MOSI_opticalDriver_DM0                    (* (reg8 *) MOSI_opticalDriver__DM0) 
#define MOSI_opticalDriver_DM1                    (* (reg8 *) MOSI_opticalDriver__DM1)
#define MOSI_opticalDriver_DM2                    (* (reg8 *) MOSI_opticalDriver__DM2) 
/* Input Buffer Disable Override */
#define MOSI_opticalDriver_INP_DIS                (* (reg8 *) MOSI_opticalDriver__INP_DIS)
/* LCD Common or Segment Drive */
#define MOSI_opticalDriver_LCD_COM_SEG            (* (reg8 *) MOSI_opticalDriver__LCD_COM_SEG)
/* Enable Segment LCD */
#define MOSI_opticalDriver_LCD_EN                 (* (reg8 *) MOSI_opticalDriver__LCD_EN)
/* Slew Rate Control */
#define MOSI_opticalDriver_SLW                    (* (reg8 *) MOSI_opticalDriver__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define MOSI_opticalDriver_PRTDSI__CAPS_SEL       (* (reg8 *) MOSI_opticalDriver__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define MOSI_opticalDriver_PRTDSI__DBL_SYNC_IN    (* (reg8 *) MOSI_opticalDriver__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define MOSI_opticalDriver_PRTDSI__OE_SEL0        (* (reg8 *) MOSI_opticalDriver__PRTDSI__OE_SEL0) 
#define MOSI_opticalDriver_PRTDSI__OE_SEL1        (* (reg8 *) MOSI_opticalDriver__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define MOSI_opticalDriver_PRTDSI__OUT_SEL0       (* (reg8 *) MOSI_opticalDriver__PRTDSI__OUT_SEL0) 
#define MOSI_opticalDriver_PRTDSI__OUT_SEL1       (* (reg8 *) MOSI_opticalDriver__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define MOSI_opticalDriver_PRTDSI__SYNC_OUT       (* (reg8 *) MOSI_opticalDriver__PRTDSI__SYNC_OUT) 


#if defined(MOSI_opticalDriver__INTSTAT)  /* Interrupt Registers */

    #define MOSI_opticalDriver_INTSTAT                (* (reg8 *) MOSI_opticalDriver__INTSTAT)
    #define MOSI_opticalDriver_SNAP                   (* (reg8 *) MOSI_opticalDriver__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins MOSI_opticalDriver_H */


/* [] END OF FILE */
