/*******************************************************************************
* File Name: SCLK_opticalDriver.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SCLK_opticalDriver_H) /* Pins SCLK_opticalDriver_H */
#define CY_PINS_SCLK_opticalDriver_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "SCLK_opticalDriver_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    SCLK_opticalDriver_Write(uint8 value) ;
void    SCLK_opticalDriver_SetDriveMode(uint8 mode) ;
uint8   SCLK_opticalDriver_ReadDataReg(void) ;
uint8   SCLK_opticalDriver_Read(void) ;
uint8   SCLK_opticalDriver_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define SCLK_opticalDriver_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define SCLK_opticalDriver_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define SCLK_opticalDriver_DM_RES_UP          PIN_DM_RES_UP
#define SCLK_opticalDriver_DM_RES_DWN         PIN_DM_RES_DWN
#define SCLK_opticalDriver_DM_OD_LO           PIN_DM_OD_LO
#define SCLK_opticalDriver_DM_OD_HI           PIN_DM_OD_HI
#define SCLK_opticalDriver_DM_STRONG          PIN_DM_STRONG
#define SCLK_opticalDriver_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define SCLK_opticalDriver_MASK               SCLK_opticalDriver__MASK
#define SCLK_opticalDriver_SHIFT              SCLK_opticalDriver__SHIFT
#define SCLK_opticalDriver_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define SCLK_opticalDriver_PS                     (* (reg8 *) SCLK_opticalDriver__PS)
/* Data Register */
#define SCLK_opticalDriver_DR                     (* (reg8 *) SCLK_opticalDriver__DR)
/* Port Number */
#define SCLK_opticalDriver_PRT_NUM                (* (reg8 *) SCLK_opticalDriver__PRT) 
/* Connect to Analog Globals */                                                  
#define SCLK_opticalDriver_AG                     (* (reg8 *) SCLK_opticalDriver__AG)                       
/* Analog MUX bux enable */
#define SCLK_opticalDriver_AMUX                   (* (reg8 *) SCLK_opticalDriver__AMUX) 
/* Bidirectional Enable */                                                        
#define SCLK_opticalDriver_BIE                    (* (reg8 *) SCLK_opticalDriver__BIE)
/* Bit-mask for Aliased Register Access */
#define SCLK_opticalDriver_BIT_MASK               (* (reg8 *) SCLK_opticalDriver__BIT_MASK)
/* Bypass Enable */
#define SCLK_opticalDriver_BYP                    (* (reg8 *) SCLK_opticalDriver__BYP)
/* Port wide control signals */                                                   
#define SCLK_opticalDriver_CTL                    (* (reg8 *) SCLK_opticalDriver__CTL)
/* Drive Modes */
#define SCLK_opticalDriver_DM0                    (* (reg8 *) SCLK_opticalDriver__DM0) 
#define SCLK_opticalDriver_DM1                    (* (reg8 *) SCLK_opticalDriver__DM1)
#define SCLK_opticalDriver_DM2                    (* (reg8 *) SCLK_opticalDriver__DM2) 
/* Input Buffer Disable Override */
#define SCLK_opticalDriver_INP_DIS                (* (reg8 *) SCLK_opticalDriver__INP_DIS)
/* LCD Common or Segment Drive */
#define SCLK_opticalDriver_LCD_COM_SEG            (* (reg8 *) SCLK_opticalDriver__LCD_COM_SEG)
/* Enable Segment LCD */
#define SCLK_opticalDriver_LCD_EN                 (* (reg8 *) SCLK_opticalDriver__LCD_EN)
/* Slew Rate Control */
#define SCLK_opticalDriver_SLW                    (* (reg8 *) SCLK_opticalDriver__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define SCLK_opticalDriver_PRTDSI__CAPS_SEL       (* (reg8 *) SCLK_opticalDriver__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define SCLK_opticalDriver_PRTDSI__DBL_SYNC_IN    (* (reg8 *) SCLK_opticalDriver__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define SCLK_opticalDriver_PRTDSI__OE_SEL0        (* (reg8 *) SCLK_opticalDriver__PRTDSI__OE_SEL0) 
#define SCLK_opticalDriver_PRTDSI__OE_SEL1        (* (reg8 *) SCLK_opticalDriver__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define SCLK_opticalDriver_PRTDSI__OUT_SEL0       (* (reg8 *) SCLK_opticalDriver__PRTDSI__OUT_SEL0) 
#define SCLK_opticalDriver_PRTDSI__OUT_SEL1       (* (reg8 *) SCLK_opticalDriver__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define SCLK_opticalDriver_PRTDSI__SYNC_OUT       (* (reg8 *) SCLK_opticalDriver__PRTDSI__SYNC_OUT) 


#if defined(SCLK_opticalDriver__INTSTAT)  /* Interrupt Registers */

    #define SCLK_opticalDriver_INTSTAT                (* (reg8 *) SCLK_opticalDriver__INTSTAT)
    #define SCLK_opticalDriver_SNAP                   (* (reg8 *) SCLK_opticalDriver__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins SCLK_opticalDriver_H */


/* [] END OF FILE */
