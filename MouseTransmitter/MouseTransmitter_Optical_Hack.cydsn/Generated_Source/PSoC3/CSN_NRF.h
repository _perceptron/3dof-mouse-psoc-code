/*******************************************************************************
* File Name: CSN_NRF.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CSN_NRF_H) /* Pins CSN_NRF_H */
#define CY_PINS_CSN_NRF_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "CSN_NRF_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    CSN_NRF_Write(uint8 value) ;
void    CSN_NRF_SetDriveMode(uint8 mode) ;
uint8   CSN_NRF_ReadDataReg(void) ;
uint8   CSN_NRF_Read(void) ;
uint8   CSN_NRF_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define CSN_NRF_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define CSN_NRF_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define CSN_NRF_DM_RES_UP          PIN_DM_RES_UP
#define CSN_NRF_DM_RES_DWN         PIN_DM_RES_DWN
#define CSN_NRF_DM_OD_LO           PIN_DM_OD_LO
#define CSN_NRF_DM_OD_HI           PIN_DM_OD_HI
#define CSN_NRF_DM_STRONG          PIN_DM_STRONG
#define CSN_NRF_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define CSN_NRF_MASK               CSN_NRF__MASK
#define CSN_NRF_SHIFT              CSN_NRF__SHIFT
#define CSN_NRF_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define CSN_NRF_PS                     (* (reg8 *) CSN_NRF__PS)
/* Data Register */
#define CSN_NRF_DR                     (* (reg8 *) CSN_NRF__DR)
/* Port Number */
#define CSN_NRF_PRT_NUM                (* (reg8 *) CSN_NRF__PRT) 
/* Connect to Analog Globals */                                                  
#define CSN_NRF_AG                     (* (reg8 *) CSN_NRF__AG)                       
/* Analog MUX bux enable */
#define CSN_NRF_AMUX                   (* (reg8 *) CSN_NRF__AMUX) 
/* Bidirectional Enable */                                                        
#define CSN_NRF_BIE                    (* (reg8 *) CSN_NRF__BIE)
/* Bit-mask for Aliased Register Access */
#define CSN_NRF_BIT_MASK               (* (reg8 *) CSN_NRF__BIT_MASK)
/* Bypass Enable */
#define CSN_NRF_BYP                    (* (reg8 *) CSN_NRF__BYP)
/* Port wide control signals */                                                   
#define CSN_NRF_CTL                    (* (reg8 *) CSN_NRF__CTL)
/* Drive Modes */
#define CSN_NRF_DM0                    (* (reg8 *) CSN_NRF__DM0) 
#define CSN_NRF_DM1                    (* (reg8 *) CSN_NRF__DM1)
#define CSN_NRF_DM2                    (* (reg8 *) CSN_NRF__DM2) 
/* Input Buffer Disable Override */
#define CSN_NRF_INP_DIS                (* (reg8 *) CSN_NRF__INP_DIS)
/* LCD Common or Segment Drive */
#define CSN_NRF_LCD_COM_SEG            (* (reg8 *) CSN_NRF__LCD_COM_SEG)
/* Enable Segment LCD */
#define CSN_NRF_LCD_EN                 (* (reg8 *) CSN_NRF__LCD_EN)
/* Slew Rate Control */
#define CSN_NRF_SLW                    (* (reg8 *) CSN_NRF__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define CSN_NRF_PRTDSI__CAPS_SEL       (* (reg8 *) CSN_NRF__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define CSN_NRF_PRTDSI__DBL_SYNC_IN    (* (reg8 *) CSN_NRF__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define CSN_NRF_PRTDSI__OE_SEL0        (* (reg8 *) CSN_NRF__PRTDSI__OE_SEL0) 
#define CSN_NRF_PRTDSI__OE_SEL1        (* (reg8 *) CSN_NRF__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define CSN_NRF_PRTDSI__OUT_SEL0       (* (reg8 *) CSN_NRF__PRTDSI__OUT_SEL0) 
#define CSN_NRF_PRTDSI__OUT_SEL1       (* (reg8 *) CSN_NRF__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define CSN_NRF_PRTDSI__SYNC_OUT       (* (reg8 *) CSN_NRF__PRTDSI__SYNC_OUT) 


#if defined(CSN_NRF__INTSTAT)  /* Interrupt Registers */

    #define CSN_NRF_INTSTAT                (* (reg8 *) CSN_NRF__INTSTAT)
    #define CSN_NRF_SNAP                   (* (reg8 *) CSN_NRF__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins CSN_NRF_H */


/* [] END OF FILE */
