/*******************************************************************************
* File Name: NRF_CSN.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "NRF_CSN.h"


/*******************************************************************************
* Function Name: NRF_CSN_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void NRF_CSN_Write(uint8 value) 
{
    uint8 staticBits = (NRF_CSN_DR & (uint8)(~NRF_CSN_MASK));
    NRF_CSN_DR = staticBits | ((uint8)(value << NRF_CSN_SHIFT) & NRF_CSN_MASK);
}


/*******************************************************************************
* Function Name: NRF_CSN_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void NRF_CSN_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(NRF_CSN_0, mode);
}


/*******************************************************************************
* Function Name: NRF_CSN_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro NRF_CSN_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 NRF_CSN_Read(void) 
{
    return (NRF_CSN_PS & NRF_CSN_MASK) >> NRF_CSN_SHIFT;
}


/*******************************************************************************
* Function Name: NRF_CSN_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 NRF_CSN_ReadDataReg(void) 
{
    return (NRF_CSN_DR & NRF_CSN_MASK) >> NRF_CSN_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(NRF_CSN_INTSTAT) 

    /*******************************************************************************
    * Function Name: NRF_CSN_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 NRF_CSN_ClearInterrupt(void) 
    {
        return (NRF_CSN_INTSTAT & NRF_CSN_MASK) >> NRF_CSN_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
