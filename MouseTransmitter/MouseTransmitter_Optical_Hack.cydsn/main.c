/* ========================================
nRF24L01 specifics 
* ========================================
SPI response should be 0x0E - reset value
CE Pin controls TX and RX on the chip. Active high.
*/


#include <project.h>
#include <stdlib.h>
#include "nRF24L01.h"
#include "const.h"
#include "opticalDriver.h"
#include "opticalDriver2.h"


#define EZI2CBUFFERSIZE 5
#define DEBUG_ENABLED 0
#define EFFICIENT_FILTER 0
#define DATA_DUMP_ENABLED 0
#define EZI2C_ENABLED 1
#define DELTA_T_INVERSE 1
#define X_REPORT_ENABLED 1
#define Y_REPORT_ENABLED 1
#define GYRO_REPORT_ENABLED 0


#define ADC_CHANNEL_COUNT 4
#define BUFFER_SIZE_PER_CHANNEL 32 
#define DMA_TD_LENGTH (2*BUFFER_SIZE_PER_CHANNEL*ADC_CHANNEL_COUNT)
#define FILTER_SIZE 32

#define ADC_SAMPLER_PERIOD 499
#define ADC_SAMPLER_COMPARE 250

/* DMA Configuration for DMA */
#define DMA_BYTES_PER_BURST 2
#define DMA_REQUEST_PER_BURST 1
#define DMA_SRC_BASE (CYDEV_PERIPH_BASE)
#define DMA_DST_BASE (CYDEV_SRAM_BASE)

#define MOUSE_LEFT_CLICKED 0x01
#define MOUSE_RIGHT_CLICKED 0x02
#define ROTATION_SCALING_FACTOR 1 

int8 ezi2cSource[EZI2CBUFFERSIZE];
uint8 channelNumber = 0;
uint16 time = 0;

void myi2c_init();
void adcInit();
void initAll(void);
void InitializeDma(void);
void initializeAdcHardware(void);
void sampleAdc(void);
void blink(uint8 repetition);
void performFiltering(void);
void squareMouse(void);
void transmit(uint8 *Data, int Data_Len);
void integrateToVelocity(void);
void integrateToDisplacement(void);
void computeDcOffset(void);
void performInitialFiltering(void);
void restartSampling(void);
void findIfStatic(void);
int16 scaleAndMap(int16 diffInDisp);
void enableTransmission(void);
int16 scaleAndMapGyro(void);
void applyHighPassFilter(void);
int TX_PL_Write( uint8 *Data, int Data_Len);

static int8 MouseData[4] = {0, 0, 0, 0}; 

uint16 xRawAcceleration = 0;
uint16 yRawAcceleration = 0;
uint16 zRawAcceleration = 0;
uint16 gyroRawValue = 0;

uint16 xFilteredAccelerationBuffer[64] = {0};
uint16 yFilteredAccelerationBuffer[64] = {0};
uint16 zFilteredAccelerationBuffer[64] = {0};
uint16 gyroFilteredBuffer[64] = {0};

uint32 xFiltered = 0;
uint32 yFiltered = 0;
uint32 zFiltered = 0;
uint32 gyroFiltered = 0;

uint8 newestFilteredDataIndex = 0;
uint8 oldestFilteredDataIndex = 1;

uint8 newestAdcSampleIndex = BUFFER_SIZE_PER_CHANNEL - 1;
uint8 oldestAdcSampleIndex = 0;

static uint16 myDelay = 0;

uint8 peformFilterFlag = 0;
uint8 initiateTransmissionFlag = 0;

uint8 isStatic = 0x01;
uint8 epsilon = 16;

/* Variable declarations for DMA */
/* Move these variable declarations to the top of the function */
uint8 DMA_Chan;
uint8 DMA_TD[1];
uint16 ADC_Samples[BUFFER_SIZE_PER_CHANNEL*ADC_CHANNEL_COUNT]; /* An array of length 32 with each containing three 16-bit words */ 

int8 mouseDataDump[512];
uint16 dataDumpIndex = 0;

int8 deltaX1, deltaY1;
int8 deltaX2, deltaY2;
float t = .3;
float rotation = 0.0;
uint8 mouseLeftClickFlag = 0;
uint8 mouseRightClickFlag = 0;
float Y0minusY1 = 42;
float multiplierExperimental = 376.254;
float tau = 0;
float a =0;
float b = 0;

void applyThetaCorrection();

int main()
{
    uint8 status;
    char* helloString = "hello";
    uint8 i=0;
    uint8 datacounter =0;
    uint8 deltaX = 0;
    
    
    initAll();
    
    //CE_LOW();
    //status = SPI_Send_command_without_ADDR(FLUSH_TX, NOP);
    
    #if DEBUG_ENABLED
    LCD_Position(0,0);
    LCD_PrintNumber(status);
    #endif
    
    enableTransmission();    
    
    for(;;)
    {
         
        if(initiateTransmissionFlag == 1)
        {
        initiateTransmissionFlag = 0;
        isr_transmission_control_ClearPending();
        
        #if X_REPORT_ENABLED
            
            if(opticalDriverIsMouseMoving() == MOUSE_MOVING)
            {
                deltaX1 = (int8)(-opticalDriverGetDeltaY());
                deltaY1 = (int8)(opticalDriverGetDeltaX());
            }

            
            if(opticalDriverIsMouseMoving_2() == MOUSE_MOVING)
            {
              //  deltaX1 = (int8)(-opticalDriverGetDeltaY());
              //  deltaY1 = (int8)(opticalDriverGetDeltaX());
                
                deltaX2 = (int8)(opticalDriverGetDeltaY_2());
                deltaY2 = (int8)(-opticalDriverGetDeltaX_2());
            }

        #endif
        
       
        
        if(mouseLeftClickFlag && (abs(deltaX1 - deltaX2) > 1   && abs(deltaY1 - deltaY2) > 0 ))
        //if((( deltaX1>0 && deltaX2<0 ) || (deltaX1<0 && deltaX2>0)||( deltaY1>0 && deltaY2<0 ) || (deltaY1<0 && deltaY2>0) ))
        //if(mouseLeftClickFlag)
        {
            //if(((float)deltaY1 - (float)deltaY2) != 0)
            if(deltaX1>deltaX2)
            rotation = ROTATION_SCALING_FACTOR*((float)(deltaX2 - 0.82*deltaX1 ));
            
            else
            rotation = ROTATION_SCALING_FACTOR*((float)(0.82*deltaX2 - deltaX1 ));///((float)(deltaY2 - deltaY1));

            //tau = ((float)( -deltaX1 + deltaX2))/(multiplierExperimental*Y0minusY1);
//            deltaX1 = 0;
//            deltaY1 = 0;
//            deltaX2 = 0;
//            deltaY2 = 0;
        }
        
         if ((Pin_Left_Click_Read() == 0) && (mouseLeftClickFlag == 0))  // if the joystick button is pressed
          {
            mouseLeftClickFlag = 1;
            MouseData[0] |= MOUSE_LEFT_CLICKED;  // click the left button down
          }
          else if ((Pin_Left_Click_Read() == 0x01)&&(mouseLeftClickFlag == 1)) // if the joystick button is not pressed
          {
            mouseLeftClickFlag = 0;
            MouseData[0] &= ~MOUSE_LEFT_CLICKED;;  // release the left button
          }
        
        
    
        
          if ((Pin_Right_Click_Read() == 0) && (mouseRightClickFlag == 0))  // if the joystick button is pressed
          {
            mouseRightClickFlag = 1;
            MouseData[0] |= MOUSE_RIGHT_CLICKED;  // click the left button down
          }
          else if ((Pin_Right_Click_Read() == 0x01)&&(mouseRightClickFlag == 1)) // if the joystick button is not pressed
          {
            mouseRightClickFlag = 0;
            MouseData[0] &= ~MOUSE_RIGHT_CLICKED;;  // release the left button
          }
        
        MouseData [1] = (t*deltaX2+(1-t)*deltaX1)/1.5;
        MouseData [2] = (t*deltaY2+(1-t)*deltaY1)/1.5;
        MouseData [3] =  (int8) rotation;
//          MouseData [1] = deltaX1 + tau * 76;
//        MouseData [2] = deltaY1 -  tau*44;
//        MouseData [3] =  (int8) tau;

        
//        applyThetaCorrection();
        
        transmit((uint8*)MouseData, 4); 
        
        #if EZI2C_ENABLED
            ezi2cSource[0] = deltaX1;
            ezi2cSource[1] = deltaY1;
            ezi2cSource[2] = deltaX2;
            ezi2cSource[3] = deltaY2;
            ezi2cSource[4] = (int8) rotation;
        #endif
        
        deltaX1 = 0;
        deltaY1 = 0;
        deltaX2 = 0;
        deltaY2 = 0;
        rotation = 0;
        tau = 0;
        
        }
    }
}



void initAll(void){
    CyGlobalIntEnable; 
    PWM_heartBeat_Start();
   
    #if EZI2C_ENABLED 
    myi2c_init();
    #endif
       
    SPIM_NRF_Controller_Start();
    nRFTXSetting(); 
    setupOpticalSensor(); 
    setupOpticalSensor_2(); 
}


void myi2c_init(){
    EZI2C_1_Start();
    EZI2C_1_EnableInt();
    EZI2C_1_SetAddress1(0x08);
    EZI2C_1_SetBuffer1(EZI2CBUFFERSIZE,0,(void *)ezi2cSource);
}

void enableTransmission()
{
    Control_Reg_Start_Transmission_Write(0x00);
    PWM_Transmission_Control_Start();
    isr_transmission_control_Start();
}


int TX_PL_Write( uint8 *Data, int Data_Len){   // can write up to 32 Bytes to the TX FIFO of the transmitter
  char temp;
  int j;
  CSN_LOW();
  SPI_SendByte(W_TX_PAYLOAD);
  for (j=0;j<(Data_Len);j++){
    SPI_SendByte(*(Data+j));
    temp = *(Data+j);
  }
  CSN_HIGH();
  return ERROR_NO_ERROR;
}

void transmit(uint8 *Data, int Data_Len)
    {
        uint8 status;
        uint8 status_temp;
        
        TX_PL_Write((uint8*)Data,Data_Len);
        
        CE_HIGH();
        CE_LOW();
    
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        while ((status  & TX_DS) != TX_DS ){
        if ((status & MAX_RT) != 0) {

        // Clear MAX_RT bit in status register
        status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|MAX_RT));
        #if DEBUG_ENABLED
        LCD_Position(0,0);
        LCD_PrintString("Packet lost");//Toggle LED1 to indicate that a packet is lost during Transmission
        blink(1);
        #endif 
        // No communication event here
        // ....
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        break;
        }
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        }

        if ((status & TX_DS) != 0) {//One packet of data sent successfully
        // Clear TX_DS bit in status register 
        status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|TX_DS));
        #if DEBUG_ENABLED
        LCD_Position(0,0);
        LCD_PrintString("1 Packet sent");
        blink(0);
        #endif
        }
        
        if ((status & MAX_RT) != 0) 
        {
        // Clear TX_DS bit in status register
        status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|MAX_RT));
        #if DEBUG_ENABLED
        LCD_Position(0,0);
        LCD_PrintString("MAX_RT is cleared");
        blink(3);
        #endif
        }

        if ((status & TX_FULL) != 0)  {
          // Flush TX FIFO (in TX mode          
          status_temp = SPI_Send_command_without_ADDR(FLUSH_TX, NOP);
        }
       status = SPI_Send_command_without_ADDR(NOP,NOP);
        
    }

    
void applyThetaCorrection()
{
    int8 originalDeltaX =  MouseData[1];
    int8 localTheta = (deltaX1 - deltaX2);
    MouseData[1] = MouseData[1] - rotation * MouseData[2];  
    MouseData[2] = rotation * originalDeltaX +  MouseData[2];
}

/* [] END OF FILE */
