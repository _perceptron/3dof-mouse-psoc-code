/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "opticalDriver.h"
#include "SPIM_opticalDriver.h"
#include "cytypes.h"
#include "opticalDriver_NCS.h"
#include "opticalDriver_Reset.h"


int dumpWidth = 256; // Number of pixels to read for each frame.
uint8 frame[256];
uint8 productId;
uint8 revisionId;

// Chip Select -> high
void NCS_HIGH  (void){
    opticalDriver_NCS_Write(0x01);    
}
// Chip Select -> low
void NCS_LOW (void){
    opticalDriver_NCS_Write(0x00);
}

uint8 SPI_opticalDriver_SendByte(uint8 spiData){

    uint8 rxStatus = 0x00;
    SPIM_opticalDriver_WriteTxData(spiData);
    rxStatus = SPIM_opticalDriver_RX_STATUS_REG;
    while((rxStatus&SPIM_opticalDriver_STS_RX_FIFO_NOT_EMPTY) != SPIM_opticalDriver_STS_RX_FIFO_NOT_EMPTY){
        rxStatus = SPIM_opticalDriver_RX_STATUS_REG;
    }
    return (uint8)SPIM_opticalDriver_ReadRxData()  ;

}


void opticalDriverWriteRegister(uint8 address, uint8 regData) {
    uint8 status;
    address |= 0x80; // MSB indicates write mode.
  
    NCS_LOW();
    status = SPI_opticalDriver_SendByte(address);
    CyDelayUs(40);
    status = SPI_opticalDriver_SendByte(regData);
    NCS_HIGH();
    CyDelayUs(40); // tSWW = 30us , tSWR = 20us min.
}


uint8 opticalDriverReadRegister(uint8 address) {

    uint8 status;
    
    NCS_LOW();
    status = SPI_opticalDriver_SendByte(address);
    CyDelayUs(10); //tSRAD = 4us min.
    status = SPI_opticalDriver_SendByte(0x00); //dummy write to pump data out of the sensor
    NCS_HIGH();
    CyDelayUs(2); // tSRR, tSRR = 250ns min.  
  
    return status;
}


void reset() {
    //opticalDriverWriteRegister(REG_CHIP_RESET,0x5A); //Write 0x5a to initiate chip RESET.
    opticalDriver_Reset_Write(0);
    CyDelayUs(20);
    opticalDriver_Reset_Write(1);
}

void setupOpticalSensor() {
  //Serial.begin(115200);
    SPIM_opticalDriver_Start();
    reset();
    productId = opticalDriverReadRegister(REG_PRODUCT_ID);
    revisionId = opticalDriverReadRegister(REG_REVISION_ID);
    opticalDriverWriteRegister(0x0d,0x01);

}

uint8 opticalDriverGetDeltaX(){
    return opticalDriverReadRegister(REG_DELTA_X);
}

uint8 opticalDriverGetDeltaY(){
    return opticalDriverReadRegister(REG_DELTA_Y);
}

uint8 opticalDriverGetProductId(void)
{
    return opticalDriverReadRegister(REG_PRODUCT_ID);
}

uint8 opticalDriverIsMouseMoving(void)
{
    uint8 status = opticalDriverReadRegister(REG_MOTION);
    
    if((status&0x80) == 0x80)
    {
        return MOUSE_MOVING;
    }
    
    else
    return MOUSE_NOT_MOVING;
}

/* [] END OF FILE */
