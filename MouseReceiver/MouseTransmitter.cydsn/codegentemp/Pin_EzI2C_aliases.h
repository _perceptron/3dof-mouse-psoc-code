/*******************************************************************************
* File Name: Pin_EzI2C.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pin_EzI2C_ALIASES_H) /* Pins Pin_EzI2C_ALIASES_H */
#define CY_PINS_Pin_EzI2C_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define Pin_EzI2C_0		Pin_EzI2C__0__PC
#define Pin_EzI2C_1		Pin_EzI2C__1__PC

#define Pin_EzI2C_scl		Pin_EzI2C__scl__PC
#define Pin_EzI2C_sda		Pin_EzI2C__sda__PC

#endif /* End Pins Pin_EzI2C_ALIASES_H */


/* [] END OF FILE */
