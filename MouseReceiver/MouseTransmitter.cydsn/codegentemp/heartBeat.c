/*******************************************************************************
* File Name: heartBeat.c  
* Version 3.0
*
* Description:
*  The PWM User Module consist of an 8 or 16-bit counter with two 8 or 16-bit
*  comparitors. Each instance of this user module is capable of generating
*  two PWM outputs with the same period. The pulse width is selectable between
*  1 and 255/65535. The period is selectable between 2 and 255/65536 clocks. 
*  The compare value output may be configured to be active when the present 
*  counter is less than or less than/equal to the compare value.
*  A terminal count output is also provided. It generates a pulse one clock
*  width wide when the counter is equal to zero.
*
* Note:
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "heartBeat.h"

uint8 heartBeat_initVar = 0u;


/*******************************************************************************
* Function Name: heartBeat_Start
********************************************************************************
*
* Summary:
*  The start function initializes the pwm with the default values, the 
*  enables the counter to begin counting.  It does not enable interrupts,
*  the EnableInt command should be called if interrupt generation is required.
*
* Parameters:  
*  None  
*
* Return: 
*  None
*
* Global variables:
*  heartBeat_initVar: Is modified when this function is called for the 
*   first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void heartBeat_Start(void) 
{
    /* If not Initialized then initialize all required hardware and software */
    if(heartBeat_initVar == 0u)
    {
        heartBeat_Init();
        heartBeat_initVar = 1u;
    }
    heartBeat_Enable();

}


/*******************************************************************************
* Function Name: heartBeat_Init
********************************************************************************
*
* Summary:
*  Initialize component's parameters to the parameters set by user in the 
*  customizer of the component placed onto schematic. Usually called in 
*  heartBeat_Start().
*
* Parameters:  
*  None
*
* Return: 
*  None
*
*******************************************************************************/
void heartBeat_Init(void) 
{
    #if (heartBeat_UsingFixedFunction || heartBeat_UseControl)
        uint8 ctrl;
    #endif /* (heartBeat_UsingFixedFunction || heartBeat_UseControl) */
    
    #if(!heartBeat_UsingFixedFunction) 
        #if(heartBeat_UseStatus)
            /* Interrupt State Backup for Critical Region*/
            uint8 heartBeat_interruptState;
        #endif /* (heartBeat_UseStatus) */
    #endif /* (!heartBeat_UsingFixedFunction) */
    
    #if (heartBeat_UsingFixedFunction)
        /* You are allowed to write the compare value (FF only) */
        heartBeat_CONTROL |= heartBeat_CFG0_MODE;
        #if (heartBeat_DeadBand2_4)
            heartBeat_CONTROL |= heartBeat_CFG0_DB;
        #endif /* (heartBeat_DeadBand2_4) */
                
        ctrl = heartBeat_CONTROL3 & ((uint8 )(~heartBeat_CTRL_CMPMODE1_MASK));
        heartBeat_CONTROL3 = ctrl | heartBeat_DEFAULT_COMPARE1_MODE;
        
         /* Clear and Set SYNCTC and SYNCCMP bits of RT1 register */
        heartBeat_RT1 &= ((uint8)(~heartBeat_RT1_MASK));
        heartBeat_RT1 |= heartBeat_SYNC;     
                
        /*Enable DSI Sync all all inputs of the PWM*/
        heartBeat_RT1 &= ((uint8)(~heartBeat_SYNCDSI_MASK));
        heartBeat_RT1 |= heartBeat_SYNCDSI_EN;
       
    #elif (heartBeat_UseControl)
        /* Set the default compare mode defined in the parameter */
        ctrl = heartBeat_CONTROL & ((uint8)(~heartBeat_CTRL_CMPMODE2_MASK)) & ((uint8)(~heartBeat_CTRL_CMPMODE1_MASK));
        heartBeat_CONTROL = ctrl | heartBeat_DEFAULT_COMPARE2_MODE | 
                                   heartBeat_DEFAULT_COMPARE1_MODE;
    #endif /* (heartBeat_UsingFixedFunction) */
        
    #if (!heartBeat_UsingFixedFunction)
        #if (heartBeat_Resolution == 8)
            /* Set FIFO 0 to 1 byte register for period*/
            heartBeat_AUX_CONTROLDP0 |= (heartBeat_AUX_CTRL_FIFO0_CLR);
        #else /* (heartBeat_Resolution == 16)*/
            /* Set FIFO 0 to 1 byte register for period */
            heartBeat_AUX_CONTROLDP0 |= (heartBeat_AUX_CTRL_FIFO0_CLR);
            heartBeat_AUX_CONTROLDP1 |= (heartBeat_AUX_CTRL_FIFO0_CLR);
        #endif /* (heartBeat_Resolution == 8) */

        heartBeat_WriteCounter(heartBeat_INIT_PERIOD_VALUE);
    #endif /* (!heartBeat_UsingFixedFunction) */
        
    heartBeat_WritePeriod(heartBeat_INIT_PERIOD_VALUE);

        #if (heartBeat_UseOneCompareMode)
            heartBeat_WriteCompare(heartBeat_INIT_COMPARE_VALUE1);
        #else
            heartBeat_WriteCompare1(heartBeat_INIT_COMPARE_VALUE1);
            heartBeat_WriteCompare2(heartBeat_INIT_COMPARE_VALUE2);
        #endif /* (heartBeat_UseOneCompareMode) */
        
        #if (heartBeat_KillModeMinTime)
            heartBeat_WriteKillTime(heartBeat_MinimumKillTime);
        #endif /* (heartBeat_KillModeMinTime) */
        
        #if (heartBeat_DeadBandUsed)
            heartBeat_WriteDeadTime(heartBeat_INIT_DEAD_TIME);
        #endif /* (heartBeat_DeadBandUsed) */

    #if (heartBeat_UseStatus || heartBeat_UsingFixedFunction)
        heartBeat_SetInterruptMode(heartBeat_INIT_INTERRUPTS_MODE);
    #endif /* (heartBeat_UseStatus || heartBeat_UsingFixedFunction) */
        
    #if (heartBeat_UsingFixedFunction)
        /* Globally Enable the Fixed Function Block chosen */
        heartBeat_GLOBAL_ENABLE |= heartBeat_BLOCK_EN_MASK;
        /* Set the Interrupt source to come from the status register */
        heartBeat_CONTROL2 |= heartBeat_CTRL2_IRQ_SEL;
    #else
        #if(heartBeat_UseStatus)
            
            /* CyEnterCriticalRegion and CyExitCriticalRegion are used to mark following region critical*/
            /* Enter Critical Region*/
            heartBeat_interruptState = CyEnterCriticalSection();
            /* Use the interrupt output of the status register for IRQ output */
            heartBeat_STATUS_AUX_CTRL |= heartBeat_STATUS_ACTL_INT_EN_MASK;
            
             /* Exit Critical Region*/
            CyExitCriticalSection(heartBeat_interruptState);
            
            /* Clear the FIFO to enable the heartBeat_STATUS_FIFOFULL
                   bit to be set on FIFO full. */
            heartBeat_ClearFIFO();
        #endif /* (heartBeat_UseStatus) */
    #endif /* (heartBeat_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: heartBeat_Enable
********************************************************************************
*
* Summary: 
*  Enables the PWM block operation
*
* Parameters:  
*  None
*
* Return: 
*  None
*
* Side Effects: 
*  This works only if software enable mode is chosen
*
*******************************************************************************/
void heartBeat_Enable(void) 
{
    /* Globally Enable the Fixed Function Block chosen */
    #if (heartBeat_UsingFixedFunction)
        heartBeat_GLOBAL_ENABLE |= heartBeat_BLOCK_EN_MASK;
        heartBeat_GLOBAL_STBY_ENABLE |= heartBeat_BLOCK_STBY_EN_MASK;
    #endif /* (heartBeat_UsingFixedFunction) */
    
    /* Enable the PWM from the control register  */
    #if (heartBeat_UseControl || heartBeat_UsingFixedFunction)
        heartBeat_CONTROL |= heartBeat_CTRL_ENABLE;
    #endif /* (heartBeat_UseControl || heartBeat_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: heartBeat_Stop
********************************************************************************
*
* Summary:
*  The stop function halts the PWM, but does not change any modes or disable
*  interrupts.
*
* Parameters:  
*  None  
*
* Return: 
*  None
*
* Side Effects:
*  If the Enable mode is set to Hardware only then this function
*  has no effect on the operation of the PWM
*
*******************************************************************************/
void heartBeat_Stop(void) 
{
    #if (heartBeat_UseControl || heartBeat_UsingFixedFunction)
        heartBeat_CONTROL &= ((uint8)(~heartBeat_CTRL_ENABLE));
    #endif /* (heartBeat_UseControl || heartBeat_UsingFixedFunction) */
    
    /* Globally disable the Fixed Function Block chosen */
    #if (heartBeat_UsingFixedFunction)
        heartBeat_GLOBAL_ENABLE &= ((uint8)(~heartBeat_BLOCK_EN_MASK));
        heartBeat_GLOBAL_STBY_ENABLE &= ((uint8)(~heartBeat_BLOCK_STBY_EN_MASK));
    #endif /* (heartBeat_UsingFixedFunction) */
}


#if (heartBeat_UseOneCompareMode)
	#if (heartBeat_CompareMode1SW)


		/*******************************************************************************
		* Function Name: heartBeat_SetCompareMode
		********************************************************************************
		* 
		* Summary:
		*  This function writes the Compare Mode for the pwm output when in Dither mode,
		*  Center Align Mode or One Output Mode.
		*
		* Parameters:
		*  comparemode:  The new compare mode for the PWM output. Use the compare types
		*                defined in the H file as input arguments.
		*
		* Return:
		*  None
		*
		*******************************************************************************/
		void heartBeat_SetCompareMode(uint8 comparemode) 
		{
		    #if(heartBeat_UsingFixedFunction)
            
                #if(0 != heartBeat_CTRL_CMPMODE1_SHIFT)
                    uint8 comparemodemasked = ((uint8)((uint8)comparemode << heartBeat_CTRL_CMPMODE1_SHIFT));
                #else
                    uint8 comparemodemasked = comparemode;
                #endif /* (0 != heartBeat_CTRL_CMPMODE1_SHIFT) */
            
	            heartBeat_CONTROL3 &= ((uint8)(~heartBeat_CTRL_CMPMODE1_MASK)); /*Clear Existing Data */
	            heartBeat_CONTROL3 |= comparemodemasked;     
		                
		    #elif (heartBeat_UseControl)
		        
                #if(0 != heartBeat_CTRL_CMPMODE1_SHIFT)
                    uint8 comparemode1masked = ((uint8)((uint8)comparemode << heartBeat_CTRL_CMPMODE1_SHIFT)) & 
    		                                    heartBeat_CTRL_CMPMODE1_MASK;
                #else
                    uint8 comparemode1masked = comparemode & heartBeat_CTRL_CMPMODE1_MASK;                
                #endif /* (0 != heartBeat_CTRL_CMPMODE1_SHIFT) */
                
                #if(0 != heartBeat_CTRL_CMPMODE2_SHIFT)                            
    		        uint8 comparemode2masked = ((uint8)((uint8)comparemode << heartBeat_CTRL_CMPMODE2_SHIFT)) & 
    		                                   heartBeat_CTRL_CMPMODE2_MASK;
                #else
    		        uint8 comparemode2masked = comparemode & heartBeat_CTRL_CMPMODE2_MASK;                
                #endif /* (0 != heartBeat_CTRL_CMPMODE2_SHIFT) */
                
		        /*Clear existing mode */
		        heartBeat_CONTROL &= ((uint8)(~(heartBeat_CTRL_CMPMODE1_MASK | heartBeat_CTRL_CMPMODE2_MASK))); 
		        heartBeat_CONTROL |= (comparemode1masked | comparemode2masked);
		        
		    #else
		        uint8 temp = comparemode;
		    #endif /* (heartBeat_UsingFixedFunction) */
		}
	#endif /* heartBeat_CompareMode1SW */

#else /* UseOneCompareMode */

	#if (heartBeat_CompareMode1SW)


		/*******************************************************************************
		* Function Name: heartBeat_SetCompareMode1
		********************************************************************************
		* 
		* Summary:
		*  This function writes the Compare Mode for the pwm or pwm1 output
		*
		* Parameters:  
		*  comparemode:  The new compare mode for the PWM output. Use the compare types
		*                defined in the H file as input arguments.
		*
		* Return: 
		*  None
		*
		*******************************************************************************/
		void heartBeat_SetCompareMode1(uint8 comparemode) 
		{
		    #if(0 != heartBeat_CTRL_CMPMODE1_SHIFT)
                uint8 comparemodemasked = ((uint8)((uint8)comparemode << heartBeat_CTRL_CMPMODE1_SHIFT)) & 
    		                               heartBeat_CTRL_CMPMODE1_MASK;
		    #else
                uint8 comparemodemasked = comparemode & heartBeat_CTRL_CMPMODE1_MASK;                
            #endif /* (0 != heartBeat_CTRL_CMPMODE1_SHIFT) */
                   
		    #if (heartBeat_UseControl)
		        heartBeat_CONTROL &= ((uint8)(~heartBeat_CTRL_CMPMODE1_MASK)); /*Clear existing mode */
		        heartBeat_CONTROL |= comparemodemasked;
		    #endif /* (heartBeat_UseControl) */
		}
	#endif /* heartBeat_CompareMode1SW */

#if (heartBeat_CompareMode2SW)


    /*******************************************************************************
    * Function Name: heartBeat_SetCompareMode2
    ********************************************************************************
    * 
    * Summary:
    *  This function writes the Compare Mode for the pwm or pwm2 output
    *
    * Parameters:  
    *  comparemode:  The new compare mode for the PWM output. Use the compare types
    *                defined in the H file as input arguments.
    *
    * Return: 
    *  None
    *
    *******************************************************************************/
    void heartBeat_SetCompareMode2(uint8 comparemode) 
    {

        #if(0 != heartBeat_CTRL_CMPMODE2_SHIFT)
            uint8 comparemodemasked = ((uint8)((uint8)comparemode << heartBeat_CTRL_CMPMODE2_SHIFT)) & 
                                                 heartBeat_CTRL_CMPMODE2_MASK;
        #else
            uint8 comparemodemasked = comparemode & heartBeat_CTRL_CMPMODE2_MASK;            
        #endif /* (0 != heartBeat_CTRL_CMPMODE2_SHIFT) */
        
        #if (heartBeat_UseControl)
            heartBeat_CONTROL &= ((uint8)(~heartBeat_CTRL_CMPMODE2_MASK)); /*Clear existing mode */
            heartBeat_CONTROL |= comparemodemasked;
        #endif /* (heartBeat_UseControl) */
    }
    #endif /*heartBeat_CompareMode2SW */

#endif /* UseOneCompareMode */


#if (!heartBeat_UsingFixedFunction)


    /*******************************************************************************
    * Function Name: heartBeat_WriteCounter
    ********************************************************************************
    * 
    * Summary:
    *  Writes a new counter value directly to the counter register. This will be 
    *  implemented for that currently running period and only that period. This API 
    *  is valid only for UDB implementation and not available for fixed function 
    *  PWM implementation.    
    *
    * Parameters:  
    *  counter:  The period new period counter value.
    *
    * Return: 
    *  None
    *
    * Side Effects: 
    *  The PWM Period will be reloaded when a counter value will be a zero
    *
    *******************************************************************************/
    void heartBeat_WriteCounter(uint8 counter) \
                                       
    {
        CY_SET_REG8(heartBeat_COUNTER_LSB_PTR, counter);
    }


    /*******************************************************************************
    * Function Name: heartBeat_ReadCounter
    ********************************************************************************
    * 
    * Summary:
    *  This function returns the current value of the counter.  It doesn't matter
    *  if the counter is enabled or running.
    *
    * Parameters:  
    *  None  
    *
    * Return: 
    *  The current value of the counter.
    *
    *******************************************************************************/
    uint8 heartBeat_ReadCounter(void) 
    {
        /* Force capture by reading Accumulator */
        /* Must first do a software capture to be able to read the counter */
        /* It is up to the user code to make sure there isn't already captured data in the FIFO */
        (void)heartBeat_COUNTERCAP_LSB;
        
        /* Read the data from the FIFO (or capture register for Fixed Function)*/
        return (CY_GET_REG8(heartBeat_CAPTURE_LSB_PTR));
    }

        #if (heartBeat_UseStatus)


            /*******************************************************************************
            * Function Name: heartBeat_ClearFIFO
            ********************************************************************************
            * 
            * Summary:
            *  This function clears all capture data from the capture FIFO
            *
            * Parameters:  
            *  None
            *
            * Return: 
            *  None
            *
            *******************************************************************************/
            void heartBeat_ClearFIFO(void) 
            {
                while(0u != (heartBeat_ReadStatusRegister() & heartBeat_STATUS_FIFONEMPTY))
                {
                    (void)heartBeat_ReadCapture();
                }
            }

        #endif /* heartBeat_UseStatus */

#endif /* !heartBeat_UsingFixedFunction */


/*******************************************************************************
* Function Name: heartBeat_WritePeriod
********************************************************************************
* 
* Summary:
*  This function is used to change the period of the counter.  The new period 
*  will be loaded the next time terminal count is detected.
*
* Parameters:  
*  period:  Period value. May be between 1 and (2^Resolution)-1.  A value of 0 
*           will result in the counter remaining at zero.
*
* Return: 
*  None
*
*******************************************************************************/
void heartBeat_WritePeriod(uint8 period) 
{
    #if(heartBeat_UsingFixedFunction)
        CY_SET_REG16(heartBeat_PERIOD_LSB_PTR, (uint16)period);
    #else
        CY_SET_REG8(heartBeat_PERIOD_LSB_PTR, period);
    #endif /* (heartBeat_UsingFixedFunction) */
}

#if (heartBeat_UseOneCompareMode)


    /*******************************************************************************
    * Function Name: heartBeat_WriteCompare
    ********************************************************************************
    * 
    * Summary:
    *  This funtion is used to change the compare1 value when the PWM is in Dither
    *  mode. The compare output will reflect the new value on the next UDB clock. 
    *  The compare output will be driven high when the present counter value is 
    *  compared to the compare value based on the compare mode defined in 
    *  Dither Mode.
    *
    * Parameters:  
    *  compare:  New compare value.  
    *
    * Return: 
    *  None
    *
    * Side Effects:
    *  This function is only available if the PWM mode parameter is set to
    *  Dither Mode, Center Aligned Mode or One Output Mode
    *
    *******************************************************************************/
    void heartBeat_WriteCompare(uint8 compare) \
                                       
    {	
		#if(heartBeat_UsingFixedFunction)
			CY_SET_REG16(heartBeat_COMPARE1_LSB_PTR, (uint16)compare);
		#else
	        CY_SET_REG8(heartBeat_COMPARE1_LSB_PTR, compare);	
		#endif /* (heartBeat_UsingFixedFunction) */
        
        #if (heartBeat_PWMMode == heartBeat__B_PWM__DITHER)
            #if(heartBeat_UsingFixedFunction)
    			CY_SET_REG16(heartBeat_COMPARE2_LSB_PTR, (uint16)(compare + 1u));
    		#else
    	        CY_SET_REG8(heartBeat_COMPARE2_LSB_PTR, (compare + 1u));	
    		#endif /* (heartBeat_UsingFixedFunction) */
        #endif /* (heartBeat_PWMMode == heartBeat__B_PWM__DITHER) */
    }


#else


    /*******************************************************************************
    * Function Name: heartBeat_WriteCompare1
    ********************************************************************************
    * 
    * Summary:
    *  This funtion is used to change the compare1 value.  The compare output will 
    *  reflect the new value on the next UDB clock.  The compare output will be 
    *  driven high when the present counter value is less than or less than or 
    *  equal to the compare register, depending on the mode.
    *
    * Parameters:  
    *  compare:  New compare value.  
    *
    * Return: 
    *  None
    *
    *******************************************************************************/
    void heartBeat_WriteCompare1(uint8 compare) \
                                        
    {
        #if(heartBeat_UsingFixedFunction)
            CY_SET_REG16(heartBeat_COMPARE1_LSB_PTR, (uint16)compare);
        #else
            CY_SET_REG8(heartBeat_COMPARE1_LSB_PTR, compare);
        #endif /* (heartBeat_UsingFixedFunction) */
    }


    /*******************************************************************************
    * Function Name: heartBeat_WriteCompare2
    ********************************************************************************
    * 
    * Summary:
    *  This funtion is used to change the compare value, for compare1 output.  
    *  The compare output will reflect the new value on the next UDB clock.  
    *  The compare output will be driven high when the present counter value is 
    *  less than or less than or equal to the compare register, depending on the 
    *  mode.
    *
    * Parameters:  
    *  compare:  New compare value.  
    *
    * Return: 
    *  None
    *
    *******************************************************************************/
    void heartBeat_WriteCompare2(uint8 compare) \
                                        
    {
        #if(heartBeat_UsingFixedFunction)
            CY_SET_REG16(heartBeat_COMPARE2_LSB_PTR, compare);
        #else
            CY_SET_REG8(heartBeat_COMPARE2_LSB_PTR, compare);
        #endif /* (heartBeat_UsingFixedFunction) */
    }
#endif /* UseOneCompareMode */

#if (heartBeat_DeadBandUsed)


    /*******************************************************************************
    * Function Name: heartBeat_WriteDeadTime
    ********************************************************************************
    * 
    * Summary:
    *  This function writes the dead-band counts to the corresponding register
    *
    * Parameters:  
    *  deadtime:  Number of counts for dead time 
    *
    * Return: 
    *  None
    *
    *******************************************************************************/
    void heartBeat_WriteDeadTime(uint8 deadtime) 
    {
        /* If using the Dead Band 1-255 mode then just write the register */
        #if(!heartBeat_DeadBand2_4)
            CY_SET_REG8(heartBeat_DEADBAND_COUNT_PTR, deadtime);
        #else
            /* Otherwise the data has to be masked and offset */        
            /* Clear existing data */
            heartBeat_DEADBAND_COUNT &= ((uint8)(~heartBeat_DEADBAND_COUNT_MASK));
            
            /* Set new dead time */
            #if(heartBeat_DEADBAND_COUNT_SHIFT)        
                heartBeat_DEADBAND_COUNT |= ((uint8)((uint8)deadtime << heartBeat_DEADBAND_COUNT_SHIFT)) & 
                                                    heartBeat_DEADBAND_COUNT_MASK;
            #else
                heartBeat_DEADBAND_COUNT |= deadtime & heartBeat_DEADBAND_COUNT_MASK;        
            #endif /* (heartBeat_DEADBAND_COUNT_SHIFT) */
        
        #endif /* (!heartBeat_DeadBand2_4) */
    }


    /*******************************************************************************
    * Function Name: heartBeat_ReadDeadTime
    ********************************************************************************
    * 
    * Summary:
    *  This function reads the dead-band counts from the corresponding register
    *
    * Parameters:  
    *  None
    *
    * Return: 
    *  Dead Band Counts
    *
    *******************************************************************************/
    uint8 heartBeat_ReadDeadTime(void) 
    {
        /* If using the Dead Band 1-255 mode then just read the register */
        #if(!heartBeat_DeadBand2_4)
            return (CY_GET_REG8(heartBeat_DEADBAND_COUNT_PTR));
        #else
        
            /* Otherwise the data has to be masked and offset */
            #if(heartBeat_DEADBAND_COUNT_SHIFT)      
                return ((uint8)(((uint8)(heartBeat_DEADBAND_COUNT & heartBeat_DEADBAND_COUNT_MASK)) >> 
                                                                           heartBeat_DEADBAND_COUNT_SHIFT));
            #else
                return (heartBeat_DEADBAND_COUNT & heartBeat_DEADBAND_COUNT_MASK);
            #endif /* (heartBeat_DEADBAND_COUNT_SHIFT) */
        #endif /* (!heartBeat_DeadBand2_4) */
    }
#endif /* DeadBandUsed */

#if (heartBeat_UseStatus || heartBeat_UsingFixedFunction)


    /*******************************************************************************
    * Function Name: heartBeat_SetInterruptMode
    ********************************************************************************
    * 
    * Summary:
    *  This function configures the interrupts mask control of theinterrupt 
    *  source status register.
    *
    * Parameters:  
    *  uint8 interruptMode: Bit field containing the interrupt sources enabled 
    *
    * Return: 
    *  None
    *
    *******************************************************************************/
    void heartBeat_SetInterruptMode(uint8 interruptMode)  
    {
    	CY_SET_REG8(heartBeat_STATUS_MASK_PTR, interruptMode);
    }


    /*******************************************************************************
    * Function Name: heartBeat_ReadStatusRegister
    ********************************************************************************
    * 
    * Summary:
    *  This function returns the current state of the status register. 
    *
    * Parameters:  
    *  None
    *
    * Return: 
    *  uint8 : Current status register value. The status register bits are:
    *  [7:6] : Unused(0)
    *  [5]   : Kill event output
    *  [4]   : FIFO not empty
    *  [3]   : FIFO full
    *  [2]   : Terminal count
    *  [1]   : Compare output 2
    *  [0]   : Compare output 1
    *
    *******************************************************************************/
    uint8 heartBeat_ReadStatusRegister(void)   
    {
    	uint8 result;
    	
    	result = CY_GET_REG8(heartBeat_STATUS_PTR);
    	return (result);
    }

#endif /* (heartBeat_UseStatus || heartBeat_UsingFixedFunction) */


#if (heartBeat_UseControl)


    /*******************************************************************************
    * Function Name: heartBeat_ReadControlRegister
    ********************************************************************************
    * 
    * Summary:
    *  Returns the current state of the control register. This API is available 
    *  only if the control register is not removed.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  uint8 : Current control register value
    *
    *******************************************************************************/    
    uint8 heartBeat_ReadControlRegister(void) 
    {
    	uint8 result;
    	
    	result = CY_GET_REG8(heartBeat_CONTROL_PTR);
    	return (result);
    }


    /*******************************************************************************
    * Function Name: heartBeat_WriteControlRegister
    ********************************************************************************
    * 
    * Summary:
    *  Sets the bit field of the control register. This API is available only if 
    *  the control register is not removed.
    *
    * Parameters:  
    *  uint8 control: Control register bit field, The status register bits are:
    *  [7]   : PWM Enable
    *  [6]   : Reset
    *  [5:3] : Compare Mode2
    *  [2:0] : Compare Mode2
    *
    * Return: 
    *  None
    *
    *******************************************************************************/  
    void heartBeat_WriteControlRegister(uint8 control) 
    {
    	CY_SET_REG8(heartBeat_CONTROL_PTR, control);
    }
	
#endif /* (heartBeat_UseControl) */


#if (!heartBeat_UsingFixedFunction)


    /*******************************************************************************
    * Function Name: heartBeat_ReadCapture
    ********************************************************************************
    * 
    * Summary:
    *  Reads the capture value from the capture FIFO.
    *
    * Parameters:  
    *  None
    *
    * Return: 
    *  uint8/uint16: The current capture value
    *
    *******************************************************************************/  
    uint8 heartBeat_ReadCapture(void)  
    {
    	return (CY_GET_REG8(heartBeat_CAPTURE_LSB_PTR));
    }
	
#endif /* (!heartBeat_UsingFixedFunction) */


#if (heartBeat_UseOneCompareMode)


    /*******************************************************************************
    * Function Name: heartBeat_ReadCompare
    ********************************************************************************
    * 
    * Summary:
    *  Reads the compare value for the compare output when the PWM Mode parameter is 
    *  set to Dither mode, Center Aligned mode, or One Output mode.
    *
    * Parameters:  
    *  None
    *
    * Return: 
    *  uint8/uint16: Current compare value
    *
    *******************************************************************************/  
    uint8 heartBeat_ReadCompare(void)  
    {
		#if(heartBeat_UsingFixedFunction)
            return ((uint8)CY_GET_REG16(heartBeat_COMPARE1_LSB_PTR));
        #else
            return (CY_GET_REG8(heartBeat_COMPARE1_LSB_PTR));
        #endif /* (heartBeat_UsingFixedFunction) */
    }

#else


    /*******************************************************************************
    * Function Name: heartBeat_ReadCompare1
    ********************************************************************************
    * 
    * Summary:
    *  Reads the compare value for the compare1 output.
    *
    * Parameters:  
    *  None
    *
    * Return: 
    *  uint8/uint16: Current compare value.
    *
    *******************************************************************************/  
    uint8 heartBeat_ReadCompare1(void) 
    {
		return (CY_GET_REG8(heartBeat_COMPARE1_LSB_PTR));
    }


    /*******************************************************************************
    * Function Name: heartBeat_ReadCompare2
    ********************************************************************************
    * 
    * Summary:
    *  Reads the compare value for the compare2 output.
    *
    * Parameters:  
    *  None
    *
    * Return: 
    *  uint8/uint16: Current compare value.
    *
    *******************************************************************************/  
    uint8 heartBeat_ReadCompare2(void)  
    {
		return (CY_GET_REG8(heartBeat_COMPARE2_LSB_PTR));
    }

#endif /* (heartBeat_UseOneCompareMode) */


/*******************************************************************************
* Function Name: heartBeat_ReadPeriod
********************************************************************************
* 
* Summary:
*  Reads the period value used by the PWM hardware.
*
* Parameters:  
*  None
*
* Return: 
*  uint8/16: Period value
*
*******************************************************************************/ 
uint8 heartBeat_ReadPeriod(void) 
{
	#if(heartBeat_UsingFixedFunction)
        return ((uint8)CY_GET_REG16(heartBeat_PERIOD_LSB_PTR));
    #else
        return (CY_GET_REG8(heartBeat_PERIOD_LSB_PTR));
    #endif /* (heartBeat_UsingFixedFunction) */
}

#if ( heartBeat_KillModeMinTime)


    /*******************************************************************************
    * Function Name: heartBeat_WriteKillTime
    ********************************************************************************
    * 
    * Summary:
    *  Writes the kill time value used by the hardware when the Kill Mode 
    *  is set to Minimum Time.
    *
    * Parameters:  
    *  uint8: Minimum Time kill counts
    *
    * Return: 
    *  None
    *
    *******************************************************************************/ 
    void heartBeat_WriteKillTime(uint8 killtime) 
    {
    	CY_SET_REG8(heartBeat_KILLMODEMINTIME_PTR, killtime);
    }


    /*******************************************************************************
    * Function Name: heartBeat_ReadKillTime
    ********************************************************************************
    * 
    * Summary:
    *  Reads the kill time value used by the hardware when the Kill Mode is set 
    *  to Minimum Time.
    *
    * Parameters:  
    *  None
    *
    * Return: 
    *  uint8: The current Minimum Time kill counts
    *
    *******************************************************************************/ 
    uint8 heartBeat_ReadKillTime(void) 
    {
    	return (CY_GET_REG8(heartBeat_KILLMODEMINTIME_PTR));
    }

#endif /* ( heartBeat_KillModeMinTime) */

/* [] END OF FILE */
