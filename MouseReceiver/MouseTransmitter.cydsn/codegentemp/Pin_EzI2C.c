/*******************************************************************************
* File Name: Pin_EzI2C.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "Pin_EzI2C.h"


/*******************************************************************************
* Function Name: Pin_EzI2C_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void Pin_EzI2C_Write(uint8 value) 
{
    uint8 staticBits = (Pin_EzI2C_DR & (uint8)(~Pin_EzI2C_MASK));
    Pin_EzI2C_DR = staticBits | ((uint8)(value << Pin_EzI2C_SHIFT) & Pin_EzI2C_MASK);
}


/*******************************************************************************
* Function Name: Pin_EzI2C_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void Pin_EzI2C_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(Pin_EzI2C_0, mode);
	CyPins_SetPinDriveMode(Pin_EzI2C_1, mode);
}


/*******************************************************************************
* Function Name: Pin_EzI2C_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro Pin_EzI2C_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 Pin_EzI2C_Read(void) 
{
    return (Pin_EzI2C_PS & Pin_EzI2C_MASK) >> Pin_EzI2C_SHIFT;
}


/*******************************************************************************
* Function Name: Pin_EzI2C_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 Pin_EzI2C_ReadDataReg(void) 
{
    return (Pin_EzI2C_DR & Pin_EzI2C_MASK) >> Pin_EzI2C_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(Pin_EzI2C_INTSTAT) 

    /*******************************************************************************
    * Function Name: Pin_EzI2C_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 Pin_EzI2C_ClearInterrupt(void) 
    {
        return (Pin_EzI2C_INTSTAT & Pin_EzI2C_MASK) >> Pin_EzI2C_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
