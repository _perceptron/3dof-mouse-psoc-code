/*******************************************************************************
* File Name: SPIM_NRF_Controller_PM.c
* Version 2.40
*
* Description:
*  This file contains the setup, control and status commands to support
*  component operations in low power mode.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "SPIM_NRF_Controller_PVT.h"

static SPIM_NRF_Controller_BACKUP_STRUCT SPIM_NRF_Controller_backup =
{
    SPIM_NRF_Controller_DISABLED,
    SPIM_NRF_Controller_BITCTR_INIT,
    #if(CY_UDB_V0)
        SPIM_NRF_Controller_TX_INIT_INTERRUPTS_MASK,
        SPIM_NRF_Controller_RX_INIT_INTERRUPTS_MASK
    #endif /* CY_UDB_V0 */
};


/*******************************************************************************
* Function Name: SPIM_NRF_Controller_SaveConfig
********************************************************************************
*
* Summary:
*  Saves SPIM configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  SPIM_NRF_Controller_backup - modified when non-retention registers are saved.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void SPIM_NRF_Controller_SaveConfig(void) 
{
    /* Store Status Mask registers */
    #if(CY_UDB_V0)
       SPIM_NRF_Controller_backup.cntrPeriod      = SPIM_NRF_Controller_COUNTER_PERIOD_REG;
       SPIM_NRF_Controller_backup.saveSrTxIntMask = SPIM_NRF_Controller_TX_STATUS_MASK_REG;
       SPIM_NRF_Controller_backup.saveSrRxIntMask = SPIM_NRF_Controller_RX_STATUS_MASK_REG;
    #endif /* (CY_UDB_V0) */
}


/*******************************************************************************
* Function Name: SPIM_NRF_Controller_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores SPIM configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  SPIM_NRF_Controller_backup - used when non-retention registers are restored.
*
* Side Effects:
*  If this API is called without first calling SaveConfig then in the following
*  registers will be default values from Customizer:
*  SPIM_NRF_Controller_STATUS_MASK_REG and SPIM_NRF_Controller_COUNTER_PERIOD_REG.
*
*******************************************************************************/
void SPIM_NRF_Controller_RestoreConfig(void) 
{
    /* Restore the data, saved by SaveConfig() function */
    #if(CY_UDB_V0)
        SPIM_NRF_Controller_COUNTER_PERIOD_REG = SPIM_NRF_Controller_backup.cntrPeriod;
        SPIM_NRF_Controller_TX_STATUS_MASK_REG = ((uint8) SPIM_NRF_Controller_backup.saveSrTxIntMask);
        SPIM_NRF_Controller_RX_STATUS_MASK_REG = ((uint8) SPIM_NRF_Controller_backup.saveSrRxIntMask);
    #endif /* (CY_UDB_V0) */
}


/*******************************************************************************
* Function Name: SPIM_NRF_Controller_Sleep
********************************************************************************
*
* Summary:
*  Prepare SPIM Component goes to sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  SPIM_NRF_Controller_backup - modified when non-retention registers are saved.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void SPIM_NRF_Controller_Sleep(void) 
{
    /* Save components enable state */
    SPIM_NRF_Controller_backup.enableState = ((uint8) SPIM_NRF_Controller_IS_ENABLED);

    SPIM_NRF_Controller_Stop();
    SPIM_NRF_Controller_SaveConfig();
}


/*******************************************************************************
* Function Name: SPIM_NRF_Controller_Wakeup
********************************************************************************
*
* Summary:
*  Prepare SPIM Component to wake up.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  SPIM_NRF_Controller_backup - used when non-retention registers are restored.
*  SPIM_NRF_Controller_txBufferWrite - modified every function call - resets to
*  zero.
*  SPIM_NRF_Controller_txBufferRead - modified every function call - resets to
*  zero.
*  SPIM_NRF_Controller_rxBufferWrite - modified every function call - resets to
*  zero.
*  SPIM_NRF_Controller_rxBufferRead - modified every function call - resets to
*  zero.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void SPIM_NRF_Controller_Wakeup(void) 
{
    SPIM_NRF_Controller_RestoreConfig();

    #if(SPIM_NRF_Controller_RX_SOFTWARE_BUF_ENABLED)
        SPIM_NRF_Controller_rxBufferFull  = 0u;
        SPIM_NRF_Controller_rxBufferRead  = 0u;
        SPIM_NRF_Controller_rxBufferWrite = 0u;
    #endif /* (SPIM_NRF_Controller_RX_SOFTWARE_BUF_ENABLED) */

    #if(SPIM_NRF_Controller_TX_SOFTWARE_BUF_ENABLED)
        SPIM_NRF_Controller_txBufferFull  = 0u;
        SPIM_NRF_Controller_txBufferRead  = 0u;
        SPIM_NRF_Controller_txBufferWrite = 0u;
    #endif /* (SPIM_NRF_Controller_TX_SOFTWARE_BUF_ENABLED) */

    /* Clear any data from the RX and TX FIFO */
    SPIM_NRF_Controller_ClearFIFO();

    /* Restore components block enable state */
    if(0u != SPIM_NRF_Controller_backup.enableState)
    {
        SPIM_NRF_Controller_Enable();
    }
}


/* [] END OF FILE */
