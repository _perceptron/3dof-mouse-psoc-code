/*******************************************************************************
* File Name: Pin_xAccelero.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pin_xAccelero_H) /* Pins Pin_xAccelero_H */
#define CY_PINS_Pin_xAccelero_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Pin_xAccelero_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    Pin_xAccelero_Write(uint8 value) ;
void    Pin_xAccelero_SetDriveMode(uint8 mode) ;
uint8   Pin_xAccelero_ReadDataReg(void) ;
uint8   Pin_xAccelero_Read(void) ;
uint8   Pin_xAccelero_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Pin_xAccelero_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define Pin_xAccelero_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define Pin_xAccelero_DM_RES_UP          PIN_DM_RES_UP
#define Pin_xAccelero_DM_RES_DWN         PIN_DM_RES_DWN
#define Pin_xAccelero_DM_OD_LO           PIN_DM_OD_LO
#define Pin_xAccelero_DM_OD_HI           PIN_DM_OD_HI
#define Pin_xAccelero_DM_STRONG          PIN_DM_STRONG
#define Pin_xAccelero_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define Pin_xAccelero_MASK               Pin_xAccelero__MASK
#define Pin_xAccelero_SHIFT              Pin_xAccelero__SHIFT
#define Pin_xAccelero_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Pin_xAccelero_PS                     (* (reg8 *) Pin_xAccelero__PS)
/* Data Register */
#define Pin_xAccelero_DR                     (* (reg8 *) Pin_xAccelero__DR)
/* Port Number */
#define Pin_xAccelero_PRT_NUM                (* (reg8 *) Pin_xAccelero__PRT) 
/* Connect to Analog Globals */                                                  
#define Pin_xAccelero_AG                     (* (reg8 *) Pin_xAccelero__AG)                       
/* Analog MUX bux enable */
#define Pin_xAccelero_AMUX                   (* (reg8 *) Pin_xAccelero__AMUX) 
/* Bidirectional Enable */                                                        
#define Pin_xAccelero_BIE                    (* (reg8 *) Pin_xAccelero__BIE)
/* Bit-mask for Aliased Register Access */
#define Pin_xAccelero_BIT_MASK               (* (reg8 *) Pin_xAccelero__BIT_MASK)
/* Bypass Enable */
#define Pin_xAccelero_BYP                    (* (reg8 *) Pin_xAccelero__BYP)
/* Port wide control signals */                                                   
#define Pin_xAccelero_CTL                    (* (reg8 *) Pin_xAccelero__CTL)
/* Drive Modes */
#define Pin_xAccelero_DM0                    (* (reg8 *) Pin_xAccelero__DM0) 
#define Pin_xAccelero_DM1                    (* (reg8 *) Pin_xAccelero__DM1)
#define Pin_xAccelero_DM2                    (* (reg8 *) Pin_xAccelero__DM2) 
/* Input Buffer Disable Override */
#define Pin_xAccelero_INP_DIS                (* (reg8 *) Pin_xAccelero__INP_DIS)
/* LCD Common or Segment Drive */
#define Pin_xAccelero_LCD_COM_SEG            (* (reg8 *) Pin_xAccelero__LCD_COM_SEG)
/* Enable Segment LCD */
#define Pin_xAccelero_LCD_EN                 (* (reg8 *) Pin_xAccelero__LCD_EN)
/* Slew Rate Control */
#define Pin_xAccelero_SLW                    (* (reg8 *) Pin_xAccelero__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Pin_xAccelero_PRTDSI__CAPS_SEL       (* (reg8 *) Pin_xAccelero__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Pin_xAccelero_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Pin_xAccelero__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Pin_xAccelero_PRTDSI__OE_SEL0        (* (reg8 *) Pin_xAccelero__PRTDSI__OE_SEL0) 
#define Pin_xAccelero_PRTDSI__OE_SEL1        (* (reg8 *) Pin_xAccelero__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Pin_xAccelero_PRTDSI__OUT_SEL0       (* (reg8 *) Pin_xAccelero__PRTDSI__OUT_SEL0) 
#define Pin_xAccelero_PRTDSI__OUT_SEL1       (* (reg8 *) Pin_xAccelero__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Pin_xAccelero_PRTDSI__SYNC_OUT       (* (reg8 *) Pin_xAccelero__PRTDSI__SYNC_OUT) 


#if defined(Pin_xAccelero__INTSTAT)  /* Interrupt Registers */

    #define Pin_xAccelero_INTSTAT                (* (reg8 *) Pin_xAccelero__INTSTAT)
    #define Pin_xAccelero_SNAP                   (* (reg8 *) Pin_xAccelero__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins Pin_xAccelero_H */


/* [] END OF FILE */
