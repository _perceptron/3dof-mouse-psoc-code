/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================


*/
/* 
/ * ========================================
nRF24L01 specifics 
 * ========================================
SPI response should be 0x0E - reset value
CE Pin controls TX and RX on the chip. Active high.
*/


#include <project.h>
#include <stdlib.h>
#include "nRF24L01.h"
#include "const.h"
#include "uartCalls.h"


#define EZI2CBUFFERSIZE 5
#define ERROR_NO_ERROR  0

uint16 ezi2cSource[EZI2CBUFFERSIZE];
uint8 channelNumber = 0;
uint16 time = 0;

void myi2c_init();
void adcInit();
void initAll(void);
void sampleAdc(void);
int TX_PL_Write( uint8 *Data, int Data_Len);
void blink(uint8 repetition);
    
int main()
{
    uint8 status;
    uint8 status_temp;
    char* helloString = "hello";
    heartBeat_Start();
    initAll();
    
    CE_LOW();
    status = SPI_Send_command_without_ADDR(FLUSH_TX, NOP);
    LCD_Position(0,0);
    LCD_PrintNumber(status);

    
    for(;;)
    {
        //sampleAdc();
        LCD_Position(0,0);
        LCD_PrintString("Transmitting byte");
        blink(2);
        status = SPI_Send_command_with_ADDR(W_REGISTER, CONFIG_REG_ADDR, 0x0A);
        TX_PL_Write((uint8*)helloString,6);
        
        CE_HIGH();
        CE_LOW();
    
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        while ((status  & TX_DS) != TX_DS ){
        if ((status & MAX_RT) != 0) {

        // Clear MAX_RT bit in status register
          status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|MAX_RT));
        LCD_Position(0,0);
        LCD_PrintString("Packet lost");//Toggle LED1 to indicate that a packet is lost during Transmission
        blink(1);
        break;
        // No communication event here
        // ....
        }
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        }

        if ((status & TX_DS) != 0) {//One packet of data sent successfully
        // Clear TX_DS bit in status register
          status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|TX_DS));
        LCD_Position(0,0);
        LCD_PrintString("1 Packet sent");
        blink(0);
        }
        
        if ((status & MAX_RT) != 0) {//One packet of data sent successfully
        // Clear TX_DS bit in status register
          status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|MAX_RT));
        LCD_Position(0,0);
        LCD_PrintString("MAX_RT is cleared");
        blink(3);
        }

        if ((status & TX_FULL) != 0)  {
          // Flush TX FIFO (in TX mode          
          status_temp = SPI_Send_command_without_ADDR(FLUSH_TX, NOP);
        }
       status = SPI_Send_command_without_ADDR(NOP,NOP);
    }
}

void myi2c_init(){
    EZI2C_1_Start();
    EZI2C_1_EnableInt();
    EZI2C_1_SetAddress1(0x08);
    EZI2C_1_SetBuffer1(2*EZI2CBUFFERSIZE,0,(void *)ezi2cSource);
}

void adcInit(){
    ADC_Start();
    ADC_StartConvert(); 
}

void initAll(void){
    CyGlobalIntEnable; 
    myi2c_init();
    adcInit();
    channelSelectMux_Start();
    SPIM_NRF_Controller_Start();
    nRFTXSetting();
  //  LCD_Start();
  //  UART_1_Start();
}


int TX_PL_Write( uint8 *Data, int Data_Len){   // can write up to 32 Bytes to the TX FIFO of the transmitter
  char temp;
  int j;
  CSN_LOW();
  SPI_SendByte(W_TX_PAYLOAD);
  for (j=0;j<(Data_Len);j++){
    SPI_SendByte(*(Data+j));
    temp = *(Data+j);
  }
  CSN_HIGH();
  return ERROR_NO_ERROR;
}

void blink(uint8 repetition)
{
    switch(repetition){
        case 0: heartBeat_WritePeriod(40);
                heartBeat_WriteCompare(20);           
        break;
        
                case 1:heartBeat_WritePeriod(150);
                heartBeat_WriteCompare(75);
        break;
        
                case 2:heartBeat_WritePeriod(255);
                heartBeat_WriteCompare(127);
        break;
                
                case 3:heartBeat_WritePeriod(20);
                heartBeat_WriteCompare(10);
        break;
        
        default:heartBeat_WritePeriod(50);
                heartBeat_WriteCompare(25);
        break;
    }
}

void sampleAdc(void)
{
     channelNumber = 0;
        channelSelectMux_Select(channelNumber);        
        ADC_StartConvert();
        while(!ADC_IsEndConversion(ADC_RETURN_STATUS))
        {
        }
        ezi2cSource[channelNumber] = ADC_GetResult16();
        ADC_StopConvert();
        
        channelNumber++;
        channelSelectMux_Select(channelNumber);
        ADC_StartConvert();
        while(!ADC_IsEndConversion(ADC_RETURN_STATUS))
        {
        }
        ezi2cSource[channelNumber] = ADC_GetResult16();
        ADC_StopConvert();
        
        channelNumber++;
        channelSelectMux_Select(channelNumber);        
        ADC_StartConvert();
        while(!ADC_IsEndConversion(ADC_RETURN_STATUS))
        {
        }
        ezi2cSource[channelNumber] = ADC_GetResult16();
        ADC_StopConvert();
        
        channelNumber++;
        channelSelectMux_Select(channelNumber);        
        ADC_StartConvert();
        while(!ADC_IsEndConversion(ADC_RETURN_STATUS))
        {
        }
        ezi2cSource[channelNumber] = ADC_GetResult16();
        ADC_StopConvert();
        
        time++;
        ezi2cSource[4] = time;
}


/* [] END OF FILE */
