/*******************************************************************************
* File Name: M_MOSI.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "M_MOSI.h"


/*******************************************************************************
* Function Name: M_MOSI_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void M_MOSI_Write(uint8 value) 
{
    uint8 staticBits = (M_MOSI_DR & (uint8)(~M_MOSI_MASK));
    M_MOSI_DR = staticBits | ((uint8)(value << M_MOSI_SHIFT) & M_MOSI_MASK);
}


/*******************************************************************************
* Function Name: M_MOSI_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void M_MOSI_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(M_MOSI_0, mode);
}


/*******************************************************************************
* Function Name: M_MOSI_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro M_MOSI_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 M_MOSI_Read(void) 
{
    return (M_MOSI_PS & M_MOSI_MASK) >> M_MOSI_SHIFT;
}


/*******************************************************************************
* Function Name: M_MOSI_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 M_MOSI_ReadDataReg(void) 
{
    return (M_MOSI_DR & M_MOSI_MASK) >> M_MOSI_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(M_MOSI_INTSTAT) 

    /*******************************************************************************
    * Function Name: M_MOSI_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 M_MOSI_ClearInterrupt(void) 
    {
        return (M_MOSI_INTSTAT & M_MOSI_MASK) >> M_MOSI_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
