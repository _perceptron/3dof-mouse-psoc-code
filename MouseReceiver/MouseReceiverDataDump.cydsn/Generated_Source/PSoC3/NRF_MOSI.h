/*******************************************************************************
* File Name: NRF_MOSI.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_NRF_MOSI_H) /* Pins NRF_MOSI_H */
#define CY_PINS_NRF_MOSI_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "NRF_MOSI_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    NRF_MOSI_Write(uint8 value) ;
void    NRF_MOSI_SetDriveMode(uint8 mode) ;
uint8   NRF_MOSI_ReadDataReg(void) ;
uint8   NRF_MOSI_Read(void) ;
uint8   NRF_MOSI_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define NRF_MOSI_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define NRF_MOSI_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define NRF_MOSI_DM_RES_UP          PIN_DM_RES_UP
#define NRF_MOSI_DM_RES_DWN         PIN_DM_RES_DWN
#define NRF_MOSI_DM_OD_LO           PIN_DM_OD_LO
#define NRF_MOSI_DM_OD_HI           PIN_DM_OD_HI
#define NRF_MOSI_DM_STRONG          PIN_DM_STRONG
#define NRF_MOSI_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define NRF_MOSI_MASK               NRF_MOSI__MASK
#define NRF_MOSI_SHIFT              NRF_MOSI__SHIFT
#define NRF_MOSI_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define NRF_MOSI_PS                     (* (reg8 *) NRF_MOSI__PS)
/* Data Register */
#define NRF_MOSI_DR                     (* (reg8 *) NRF_MOSI__DR)
/* Port Number */
#define NRF_MOSI_PRT_NUM                (* (reg8 *) NRF_MOSI__PRT) 
/* Connect to Analog Globals */                                                  
#define NRF_MOSI_AG                     (* (reg8 *) NRF_MOSI__AG)                       
/* Analog MUX bux enable */
#define NRF_MOSI_AMUX                   (* (reg8 *) NRF_MOSI__AMUX) 
/* Bidirectional Enable */                                                        
#define NRF_MOSI_BIE                    (* (reg8 *) NRF_MOSI__BIE)
/* Bit-mask for Aliased Register Access */
#define NRF_MOSI_BIT_MASK               (* (reg8 *) NRF_MOSI__BIT_MASK)
/* Bypass Enable */
#define NRF_MOSI_BYP                    (* (reg8 *) NRF_MOSI__BYP)
/* Port wide control signals */                                                   
#define NRF_MOSI_CTL                    (* (reg8 *) NRF_MOSI__CTL)
/* Drive Modes */
#define NRF_MOSI_DM0                    (* (reg8 *) NRF_MOSI__DM0) 
#define NRF_MOSI_DM1                    (* (reg8 *) NRF_MOSI__DM1)
#define NRF_MOSI_DM2                    (* (reg8 *) NRF_MOSI__DM2) 
/* Input Buffer Disable Override */
#define NRF_MOSI_INP_DIS                (* (reg8 *) NRF_MOSI__INP_DIS)
/* LCD Common or Segment Drive */
#define NRF_MOSI_LCD_COM_SEG            (* (reg8 *) NRF_MOSI__LCD_COM_SEG)
/* Enable Segment LCD */
#define NRF_MOSI_LCD_EN                 (* (reg8 *) NRF_MOSI__LCD_EN)
/* Slew Rate Control */
#define NRF_MOSI_SLW                    (* (reg8 *) NRF_MOSI__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define NRF_MOSI_PRTDSI__CAPS_SEL       (* (reg8 *) NRF_MOSI__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define NRF_MOSI_PRTDSI__DBL_SYNC_IN    (* (reg8 *) NRF_MOSI__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define NRF_MOSI_PRTDSI__OE_SEL0        (* (reg8 *) NRF_MOSI__PRTDSI__OE_SEL0) 
#define NRF_MOSI_PRTDSI__OE_SEL1        (* (reg8 *) NRF_MOSI__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define NRF_MOSI_PRTDSI__OUT_SEL0       (* (reg8 *) NRF_MOSI__PRTDSI__OUT_SEL0) 
#define NRF_MOSI_PRTDSI__OUT_SEL1       (* (reg8 *) NRF_MOSI__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define NRF_MOSI_PRTDSI__SYNC_OUT       (* (reg8 *) NRF_MOSI__PRTDSI__SYNC_OUT) 


#if defined(NRF_MOSI__INTSTAT)  /* Interrupt Registers */

    #define NRF_MOSI_INTSTAT                (* (reg8 *) NRF_MOSI__INTSTAT)
    #define NRF_MOSI_SNAP                   (* (reg8 *) NRF_MOSI__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins NRF_MOSI_H */


/* [] END OF FILE */
