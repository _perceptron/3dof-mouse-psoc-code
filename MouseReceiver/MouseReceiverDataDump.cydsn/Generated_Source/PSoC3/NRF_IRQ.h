/*******************************************************************************
* File Name: NRF_IRQ.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_NRF_IRQ_H) /* Pins NRF_IRQ_H */
#define CY_PINS_NRF_IRQ_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "NRF_IRQ_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    NRF_IRQ_Write(uint8 value) ;
void    NRF_IRQ_SetDriveMode(uint8 mode) ;
uint8   NRF_IRQ_ReadDataReg(void) ;
uint8   NRF_IRQ_Read(void) ;
uint8   NRF_IRQ_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define NRF_IRQ_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define NRF_IRQ_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define NRF_IRQ_DM_RES_UP          PIN_DM_RES_UP
#define NRF_IRQ_DM_RES_DWN         PIN_DM_RES_DWN
#define NRF_IRQ_DM_OD_LO           PIN_DM_OD_LO
#define NRF_IRQ_DM_OD_HI           PIN_DM_OD_HI
#define NRF_IRQ_DM_STRONG          PIN_DM_STRONG
#define NRF_IRQ_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define NRF_IRQ_MASK               NRF_IRQ__MASK
#define NRF_IRQ_SHIFT              NRF_IRQ__SHIFT
#define NRF_IRQ_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define NRF_IRQ_PS                     (* (reg8 *) NRF_IRQ__PS)
/* Data Register */
#define NRF_IRQ_DR                     (* (reg8 *) NRF_IRQ__DR)
/* Port Number */
#define NRF_IRQ_PRT_NUM                (* (reg8 *) NRF_IRQ__PRT) 
/* Connect to Analog Globals */                                                  
#define NRF_IRQ_AG                     (* (reg8 *) NRF_IRQ__AG)                       
/* Analog MUX bux enable */
#define NRF_IRQ_AMUX                   (* (reg8 *) NRF_IRQ__AMUX) 
/* Bidirectional Enable */                                                        
#define NRF_IRQ_BIE                    (* (reg8 *) NRF_IRQ__BIE)
/* Bit-mask for Aliased Register Access */
#define NRF_IRQ_BIT_MASK               (* (reg8 *) NRF_IRQ__BIT_MASK)
/* Bypass Enable */
#define NRF_IRQ_BYP                    (* (reg8 *) NRF_IRQ__BYP)
/* Port wide control signals */                                                   
#define NRF_IRQ_CTL                    (* (reg8 *) NRF_IRQ__CTL)
/* Drive Modes */
#define NRF_IRQ_DM0                    (* (reg8 *) NRF_IRQ__DM0) 
#define NRF_IRQ_DM1                    (* (reg8 *) NRF_IRQ__DM1)
#define NRF_IRQ_DM2                    (* (reg8 *) NRF_IRQ__DM2) 
/* Input Buffer Disable Override */
#define NRF_IRQ_INP_DIS                (* (reg8 *) NRF_IRQ__INP_DIS)
/* LCD Common or Segment Drive */
#define NRF_IRQ_LCD_COM_SEG            (* (reg8 *) NRF_IRQ__LCD_COM_SEG)
/* Enable Segment LCD */
#define NRF_IRQ_LCD_EN                 (* (reg8 *) NRF_IRQ__LCD_EN)
/* Slew Rate Control */
#define NRF_IRQ_SLW                    (* (reg8 *) NRF_IRQ__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define NRF_IRQ_PRTDSI__CAPS_SEL       (* (reg8 *) NRF_IRQ__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define NRF_IRQ_PRTDSI__DBL_SYNC_IN    (* (reg8 *) NRF_IRQ__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define NRF_IRQ_PRTDSI__OE_SEL0        (* (reg8 *) NRF_IRQ__PRTDSI__OE_SEL0) 
#define NRF_IRQ_PRTDSI__OE_SEL1        (* (reg8 *) NRF_IRQ__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define NRF_IRQ_PRTDSI__OUT_SEL0       (* (reg8 *) NRF_IRQ__PRTDSI__OUT_SEL0) 
#define NRF_IRQ_PRTDSI__OUT_SEL1       (* (reg8 *) NRF_IRQ__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define NRF_IRQ_PRTDSI__SYNC_OUT       (* (reg8 *) NRF_IRQ__PRTDSI__SYNC_OUT) 


#if defined(NRF_IRQ__INTSTAT)  /* Interrupt Registers */

    #define NRF_IRQ_INTSTAT                (* (reg8 *) NRF_IRQ__INTSTAT)
    #define NRF_IRQ_SNAP                   (* (reg8 *) NRF_IRQ__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins NRF_IRQ_H */


/* [] END OF FILE */
